package main

import (
	"encoding/binary"
	"flag"
	"net"
	"sync"

	"mtd/internal/config"
	"mtd/internal/connection/shared"
	"mtd/internal/util"
	"mtd/internal/vpn/cipher"
)

type registrationCfg struct {
	Action   string
	NodeName string
	NodeIP   string
	Mac      string
	Key      string
}

type warning struct {
	Ip     string
	Action string
	Key    string
	Reason string
}

type clientdb struct {
	sync.RWMutex
	//TUN
	Network *net.IPNet
	//UDP
	Port   int
	Protoc string
	//Cipher
	CipherKey  string
	CipherType cipher.CipherType
	//router
	byName map[string]*ClientMapElem
	byIP   map[uint32]*ClientMapElem
}

func ip2int(ip net.IP) uint32 {
	if len(ip) == 16 {
		return binary.BigEndian.Uint32(ip[12:16])
	}
	return binary.BigEndian.Uint32(ip)
}

func int2ip(nn uint32) net.IP {
	ip := make(net.IP, 4)
	binary.BigEndian.PutUint32(ip, nn)
	return ip
}

type configTriggerType int

const (
	timerTr configTriggerType = iota
	registrationTr
	deregistrationTr
	warningTr
	stopTr
)

type configTrigger struct {
	trType configTriggerType
	trData interface{}
}

var args struct {
	configPath *string
	stag       *string
}

func init() {
	args.configPath = flag.String("cfg", "./server_config.json", "The path to the configuration file")
	flag.Parse()
	config.LoadConfiguration(*args.configPath)
}

func main() {
	s := NewServer()
	s.Start()
	defer func() {
		s.Stop()
		shared.CloseAll()
	}()

	util.WaitForInterrupt()
}
