package main

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
	"mtd/internal/vpn"
	"mtd/internal/vpn/router"
	"net"

	"github.com/romana/rlog"
)

// this function generates a config with the updated client list (updates client Routing table)
func (s *Server) generateConfigFromRegistration(client *ClientMapElem, deregister bool) error {
	// client db is already updated (either registered new client or deregistered old client)
	// if client was deregistered, deregisteredClient != nil

	var sap_msg struct {
		Source  string
		Outcome string
		Key     string
	}
	if deregister {
		sap_msg.Source = "Deregistration"
	} else {
		sap_msg.Source = "Registration"
	}

	s.cldb.Lock()
	// update network
	ones, bits := s.cldb.Network.Mask.Size()

	netmaskTmp := net.CIDRMask(ones, bits)
	for i, nm := range netmaskTmp {
		netmaskTmp[i] = ^nm
	}
	ipNum := binary.BigEndian.Uint32(netmaskTmp)

	// check the available ips with the current netmask, and the registered clients
	// if the clients are more than the ips, set a bigger netmask and check again
	for ipNum < uint32(len(s.cldb.byName)) {
		ones -= 8
		netmaskTmp := net.CIDRMask(ones, bits)
		for i, nm := range netmaskTmp {
			netmaskTmp[i] = ^nm
		}
		ipNum = binary.BigEndian.Uint32(netmaskTmp)
	}
	oldOnes, _ := s.cldb.Network.Mask.Size()
	if ones != oldOnes {
		//update the network with the new netmask
		newNet := s.cldb.Network
		newNet.Mask = net.CIDRMask(ones, bits)
		rlog.Info("[generateConfigFromRegistration] Network ", s.cldb.Network.String(), " -> ", newNet.String())
		s.cldb.Network = newNet
	}

	// no need to update UDP and Cipher settings here
	// next we must assign a new unused internal ip to the new client
	ipmap := make(map[uint32]*ClientMapElem)
	clientWithoutInternalIP := false
	//first pass, fill the map with used ips
	for _, c := range s.cldb.byName {
		if !c.GetBlocked() {
			if c.InternalIP == nil || c.InternalIP.IsUnspecified() {
				clientWithoutInternalIP = true
			} else {
				ipmap[ip2int(c.InternalIP)] = c
			}
		}
	}
	if clientWithoutInternalIP {
		//second pass, assign internal ips to nodes without
		for _, c := range s.cldb.byName {
			if (c.InternalIP == nil || c.InternalIP.IsUnspecified()) && !c.GetBlocked() {
				baseIPint := ip2int(s.cldb.Network.IP)
				//search for unused internal ip
				for i := uint32(1); i < ipNum; i++ {
					tmpIPint := baseIPint + i
					rlog.Debug("[generateConfigFromRegistration] tmp IP: ", int2ip(tmpIPint))
					currClient, found := ipmap[tmpIPint]
					if !found {
						rlog.Debug("[generateConfigFromRegistration] found empty slot: ", int2ip(tmpIPint))
						ipmap[tmpIPint] = c
						c.InternalIP = int2ip(tmpIPint)
						sap_msg.Outcome = "New client added" + c.Name + " (" + c.IP.String() + ")" + c.Key
						break
					} else {
						rlog.Debug("[generateConfigFromRegistration] slot in use: ", int2ip(tmpIPint), ", ", currClient.Name)
					}
				}
			}
		}
	}

	s.cldb.Unlock()
	s.cldb.RLock()
	defer s.cldb.RUnlock()
	//third pass, init common config and create routing table
	newConfig := new(vpn.VPNConfig)

	newConfig.Cipher.IsSet = false
	newConfig.UDP.IsSet = false
	newConfig.TUN.IsSet = true
	newConfig.Router.IsSet = true

	newConfig.Router.RoutesToAdd = make([]router.Route, 0)
	newConfig.Router.RoutesToRemove = make([]string, 0)

	// RoutesToRemove is checked before RoutesToAdd inside client
	// So the InternalIP of the deregisteredClient can be reused
	if deregister {
		newConfig.Router.RoutesToRemove = append(newConfig.Router.RoutesToRemove, client.InternalIP.String())
		sap_msg.Outcome += " Client" + client.Name + " (" + client.IP.String() + ") removed"
	}

	//third pass, create routing table
	for _, c := range s.cldb.byName {
		if !c.GetBlocked() {
			newConfig.Router.RoutesToAdd = append(newConfig.Router.RoutesToAdd, router.Route{
				NodeName:   c.Name,
				LocalIP:    c.InternalIP.String(),
				ExternalIP: c.IP.String(),
			})
		}
	}
	rlog.Debug("[generateConfigFromRegistration] Routing table ", newConfig.Router.RoutesToAdd)

	//fourth pass, create personalized configs and send to clients
	for _, c := range s.cldb.byName {
		if !c.GetBlocked() {
			if !deregister && c == client {
				newConfig.Cipher.IsSet = true
				newConfig.Cipher.CipherKey = s.cldb.CipherKey
				newConfig.Cipher.CipherType = s.cldb.CipherType

				newConfig.UDP.IsSet = true
				newConfig.UDP.Port = s.cldb.Port
				newConfig.UDP.Protoc = s.cldb.Protoc

			} else {
				newConfig.Cipher.IsSet = false
				newConfig.UDP.IsSet = false
			}

			newConfig.TUN.CIDR = c.InternalIP.String() + "/" + fmt.Sprint(ones)
			newConfig.Router.ExtIP = c.IP.String()

			newConfigByte, err := json.Marshal(&newConfig)
			if err != nil {
				return fmt.Errorf("failed to marshal config: %v", err)
			}
			c.config.GetTx() <- newConfigByte
		}
	}

	sap_json, err := json.Marshal(sap_msg)
	if err != nil {
		rlog.Error("[generateConfigFromRegistration] Failed to marshal log message: ", err)
	}
	s.logChan <- string(sap_json)

	return nil
}
