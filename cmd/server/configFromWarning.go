package main

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
	"math/rand"
	"mtd/internal/config"
	"mtd/internal/connection/shared"
	"mtd/internal/vpn"
	"mtd/internal/vpn/router"
	"net"
	"time"

	"github.com/romana/rlog"
)

//this function generates a config that mitigates an attack (might isolate node)
func (s *Server) generateConfigFromWarning(delivery shared.Delivery) error {

	//TODO: verify warning source somehow (JWT)
	var sap_msg struct {
		Source  string
		Outcome string
		Key     string
	}
	s.cldb.Lock()
	// update network
	ones, bits := s.cldb.Network.Mask.Size()

	netmaskTmp := net.CIDRMask(ones, bits)
	for i, nm := range netmaskTmp {
		netmaskTmp[i] = ^nm
	}
	ipNum := binary.BigEndian.Uint32(netmaskTmp)

	w := &warning{}
	err := json.Unmarshal(delivery.GetBody(), &w)
	if err != nil {
		rlog.Error("[generateConfigFromWarning] Failed to unmarshal warning: ", err)
	}
	rlog.Infof("[generateConfigFromWarning] received warning: %+v", w)

	sap_msg.Source = delivery.GetName() + " warning: " + w.Ip + " " + w.Action + "" + w.Reason + "" + w.Key

	//TODO: define attacks and mitigations -> define warning types
	//TODO: change network - port - cipher - cipherKey
	// based on warning type and preconfigured mitigation
	//NOTE: for now support only blocking/allowing client by name

	//first pass, init common config and find blocked clients

	newConfig := new(vpn.VPNConfig)

	newConfig.Cipher.IsSet = false
	newConfig.UDP.IsSet = true
	newConfig.TUN.IsSet = true
	newConfig.Router.IsSet = true

	newConfig.Router.RoutesToAdd = make([]router.Route, 0)
	newConfig.Router.RoutesToRemove = make([]string, 0)

	node_resty := rest_msg{}

	for _, c := range s.cldb.byName {
		if c != nil {
			if w.Ip == c.IP.String() || w.Ip == c.InternalIP.String() {
				rlog.Tracef(1, "[warnings] %s -> %s %s", delivery.GetTopic(), w.Action, w.Ip, w.Reason, w.Key)
				if w.Action == "block" {
					rlog.Tracef(1, "[warnings] found client %s (%s) --- blocking %s", c.Name, c.IP, c.config.GetTopic())
					if delivery.GetName() == c.Name {
						rlog.Tracef(1, "[warnings] --- client blocked itself?")
					}
					newConfig.Router.RoutesToRemove = append(newConfig.Router.RoutesToRemove, c.Name)
					if !c.GetBlocked() {
						node_resty.name = c.Name
						node_resty.ip = c.IP.String()
						node_resty.action = w.Action
						s.updateTSNController(node_resty)
					}
					c.SetBlocked(true)
					c.InternalIP = nil
					sap_msg.Outcome = "Blocked " + c.Name + " (" + c.IP.String() + ")"
				} else if w.Action == "allow" {
					rlog.Tracef(1, "[warnings] found client ", c.Name, " (", c.IP, ") --- allowing ", c.config.GetTopic())
					sap_msg.Outcome = "Allowed " + c.Name + " (" + c.IP.String() + ")"
					if c.GetBlocked() {
						node_resty.name = c.Name
						node_resty.ip = c.IP.String()
						node_resty.action = w.Action
						s.updateTSNController(node_resty)
					}
					c.SetBlocked(false)
				}
				break
			}
		}
	}

	//second pass, update internal ips
	ipmap := make(map[uint32]*ClientMapElem)
	for _, c := range s.cldb.byName {
		if (c.InternalIP == nil || c.InternalIP.IsUnspecified()) && !c.GetBlocked() {
			baseIPint := ip2int(s.cldb.Network.IP)
			//search for unused internal ip
			for i := uint32(rand.Int()%20 + 10); i < ipNum; i++ {
				tmpIPint := baseIPint + i
				rlog.Tracef(1, "[generateConfigFromWarning] tmp IP: ", int2ip(tmpIPint))
				currClient, found := ipmap[tmpIPint]
				if !found {
					rlog.Tracef(1, "[generateConfigFromWarning] found empty slot: ", int2ip(tmpIPint))
					ipmap[tmpIPint] = c
					c.InternalIP = int2ip(tmpIPint)
					break
				} else {
					rlog.Tracef(1, "[generateConfigFromWarning] slot in use: ", int2ip(tmpIPint), " - ", currClient.Name)
				}
			}
		}
	}

	//TODO third pass, update external ips
	// update udp
	if newConfig.UDP.IsSet {
		s.cldb.Port += 10
		newConfig.UDP.IsSet = true
		newConfig.UDP.Port = s.cldb.Port
		newConfig.UDP.Protoc = s.cldb.Protoc
	} else {
		newConfig.UDP.IsSet = false
	}

	s.cldb.Unlock()
	s.cldb.RLock()
	defer s.cldb.RUnlock()

	//third pass, create routing table
	for _, c := range s.cldb.byName {
		if !c.GetBlocked() {
			newConfig.Router.RoutesToAdd = append(newConfig.Router.RoutesToAdd, router.Route{
				NodeName:   c.Name,
				LocalIP:    c.InternalIP.String(),
				ExternalIP: c.IP.String(),
			})
		}
	}
	rlog.Tracef(1, "[generateConfigFromWarning] Routing table %+v", newConfig.Router.RoutesToAdd)

	//fourth pass, create personalized configs and send to clients
	for _, c := range s.cldb.byName {
		rlog.Debugf("map elem: %s (%s)", c.Name, c.IP)
		if !c.GetBlocked() {
			newConfig.TUN.CIDR = c.InternalIP.String() + "/" + fmt.Sprint(ones)
			newConfig.Router.ExtIP = c.IP.String()
			rlog.Tracef(1, "\033[4m%s new config: %+v"+config.ClrReset, c.Name, newConfig)

			newConfigByte, err := json.Marshal(&newConfig)
			if err != nil {
				return fmt.Errorf("failed to marshal config: %v", err)
			}
			c.config.GetTx() <- newConfigByte
		}
	}

	s.configTimer.Reset(time.Second)
	sap_json, err := json.Marshal(sap_msg)
	if err != nil {
		rlog.Error("[generateConfigFromWarning] Failed to marshal log message: ", err)
	}
	rlog.Tracef(1, "SAP: %+v", sap_msg)
	s.logChan <- string(sap_json)

	return nil
}
