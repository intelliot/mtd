package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"mtd/internal/config"
	"mtd/internal/connection"
	"mtd/internal/connection/shared"
	"net"
	"time"

	"github.com/romana/rlog"
)

type ClientMapElem struct {
	Name         string
	IP           net.IP
	InternalIP   net.IP
	Blocked      bool
	Mac          string
	config       *connection.Producer
	Key          string
	kaStop       chan struct{}
	kaTimeout    *time.Ticker
	kaResponseOK chan struct{}
	kaID         string
	kaReq        *connection.Producer
	kaResp       *connection.Consumer

	idsWarn  *connection.Consumer
	idsTrust *connection.Consumer

	idsTrustMap map[string]int

	srv *Server
}

func NewClientMapElem(s *Server, r *registrationCfg) (*ClientMapElem, error) {
	c := new(ClientMapElem)
	c.Name = r.NodeName
	c.IP = net.ParseIP(r.NodeIP)
	c.Key = config.Static.Broker.GetPKey()
	if c.IP == nil {
		return nil, errors.New("new client " + r.NodeName + "(" + r.NodeIP + "): client IP format invalid")
	}
	c.InternalIP = nil
	c.Blocked = false

	//try to start new client config connection
	var cfgTopic, reqTopic, respTopic, idsWarnTopic, idsTrustTopic string
	if config.Static.Broker.Protocol == "amqp" {
		cfgTopic = "mtd.config." + r.NodeName
		reqTopic = "mtd.keepaliveReq." + r.NodeName
		respTopic = "mtd.keepaliveResp." + r.NodeName
		idsWarnTopic = "ids.warn." + r.NodeName
		idsTrustTopic = "ids.trust." + r.NodeName
	} else {
		cfgTopic = "mtd/config/" + r.NodeName
		reqTopic = "mtd/keepaliveReq/" + r.NodeName
		respTopic = "mtd/keepaliveResp/" + r.NodeName
		idsWarnTopic = "ids/warn/" + r.NodeName
		idsTrustTopic = "ids/trust/" + r.NodeName
	}

	c.config = connection.NewProducer(cfgTopic)
	c.kaStop = make(chan struct{})
	c.kaTimeout = time.NewTicker(5 * time.Second)
	c.kaResponseOK = make(chan struct{})
	c.kaID = ""
	c.kaReq = connection.NewProducer(reqTopic)
	c.kaResp = connection.NewConsumer(respTopic, c.handleNewKAResp)

	c.idsWarn = connection.NewConsumer(idsWarnTopic, c.handleNewWarnings)
	c.idsTrust = connection.NewConsumer(idsTrustTopic, c.handleNewTrust)
	c.idsTrustMap = make(map[string]int)

	c.srv = s

	return c, nil
}

func (c *ClientMapElem) Start() {

	rlog.Debug("[client ", c.Name, " config] starting config producer connection to ", c.config.GetTopic())

	go c.config.Start()

	rlog.Debug("[client ", c.Name, " keepalive] starting keepalive producer connection to ", c.kaReq.GetTopic())

	go c.kaReq.Start()

	rlog.Debug("[client ", c.Name, " keepalive] starting keepalive consumer connection to ", c.kaResp.GetTopic())

	go c.kaResp.Start()

	go c.handleNewKAReq()

	rlog.Debug("[client ", c.Name, " ids.warn] starting ids warnings consumer connection to ", c.idsWarn.GetTopic())

	go c.idsWarn.Start()

	rlog.Debug("[client ", c.Name, " ids.trust] starting ids trust consumer connection to ", c.idsTrust.GetTopic())

	go c.idsTrust.Start()
	c.config.WaitUntilReady()
	c.kaReq.WaitUntilReady()
	c.kaResp.WaitUntilReady()
	c.idsWarn.WaitUntilReady()
	c.idsTrust.WaitUntilReady()
}

func (c *ClientMapElem) Stop(sendStop bool) {
	c.config.Stop(nil)
	if sendStop {
		c.kaStop <- struct{}{}
	}
	c.kaTimeout.Stop()
	c.kaReq.Stop(nil)
	c.kaResp.Stop(nil)
	c.idsWarn.Stop(nil)
	c.idsTrust.Stop(nil)
}

func (c *ClientMapElem) SetBlocked(v bool) {
	c.Blocked = v
}

func (c *ClientMapElem) GetBlocked() bool {
	return c.Blocked
}

//this routine is started on a registration request and:
// On timer: sends keepalive request
// Waits for keepalive responce
// On success: no changes
// On failure: deregister client
func (c *ClientMapElem) handleNewKAReq() {

	shared.Wg.Add(1)
	defer shared.Wg.Done()
	rlog.Debug("[keepalive] starting ka routine")

	for {
		select {
		case <-shared.Ctx.Done():
			rlog.Debug("[keepalive] manageKeepAlive Received ctx Done, closing")
			return
		case <-c.kaStop:
			rlog.Debug("[keepalive] received close signal")
			return
		case <-c.kaTimeout.C:
			c.kaID = shared.GetCorrelationId()
			req := shared.KAReq{
				ID: c.kaID,
			}

			rlog.Debugf("Client %s Req: %+v", c.Name, req)

			rb, err := json.Marshal(&req)
			if err != nil {
				rlog.Error("[keepalive] Failed to marshal KAReq")
				continue
			}

			c.kaReq.GetTx() <- rb
			select {
			case <-c.kaResponseOK:
				rlog.Debug("[keepalive] client ok")
				continue
			case <-time.After(1 * time.Second):
				rlog.Debug("[keepalive] failed to receive answer")

				reg := registrationCfg{
					Action:   "deregister",
					NodeName: c.Name,
					NodeIP:   "",
					Mac:      c.Mac,
					Key:      c.Key,
				}
				c.srv.deregisterClient(&reg, false)
			}
		}
	}
}

func (c *ClientMapElem) handleNewKAResp(delivery shared.Delivery) error {
	resp := new(shared.KAResp)
	err := json.Unmarshal(delivery.GetBody(), &resp)
	if err != nil {
		return fmt.Errorf("failed to unmarshal new trust map: %v", err)
	}

	rlog.Debugf("Client %s Resp: %+v req ID %s", c.Name, resp, c.kaID, c.Key)
	//check if response matches request
	if resp.ID != c.kaID {
		rlog.Error("[keepalive] wrong ka response: ", c.Name, " - ", resp.ID, " != ", c.kaID, c.Key)
		return errors.New("wrong ka response")

	}
	//here other status codes can be added and checked
	if resp.Status == "OK" {
		c.kaResponseOK <- struct{}{}
	}
	return nil
}

func (c *ClientMapElem) handleNewWarnings(delivery shared.Delivery) error {
	rlog.Info("[client ", c.Name, "] received warning from ids: ", string(delivery.GetBody()))
	if !c.Blocked {
		c.srv.configTriggerChan <- configTrigger{warningTr, delivery}
	}
	return nil
}

func (c *ClientMapElem) handleNewTrust(delivery shared.Delivery) error {
	rlog.Debug("[client ", c.Name, "] received trust from ids: ", string(delivery.GetBody()))
	// create a new empty map
	newTrustMap := make(map[string]int)
	// fill the new map
	err := json.Unmarshal(delivery.GetBody(), &newTrustMap)
	if err != nil {
		return fmt.Errorf("failed to unmarshal new trust map: %v", err)
	}
	//concatenate the new and old maps, keeping the new values for existing entries
	for ip, trust := range newTrustMap {
		c.idsTrustMap[ip] = trust
	}
	rlog.Debug("[client ", c.Name, " handleNewTrust] new trust map ", c.idsTrustMap)

	return nil
}
