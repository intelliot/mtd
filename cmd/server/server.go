package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"mtd/internal/config"
	"mtd/internal/connection"
	"mtd/internal/connection/shared"
	"mtd/internal/vpn/cipher"
	"net"
	"os"
	"time"

	"github.com/go-resty/resty/v2"
	"github.com/romana/rlog"
)

type Server struct {
	TSN_URL         string
	cldb            *clientdb
	key             string
	warnings        *connection.Consumer
	registration    *connection.Consumer
	logs            *connection.Producer
	logChan         chan string
	keybytes        []byte
	configTimer     *time.Ticker
	configTimerStop chan struct{}

	configTriggerChan chan configTrigger

	iterations int
}

func NewServer() *Server {
	// init TUN network
	_, network, err := net.ParseCIDR(config.Static.VPN.TunnelCIDR)
	if err != nil {
		rlog.Critical("[Init] Failed to generate local network CIDR", err)
		os.Exit(1)
	}

	// init client database
	s := new(Server)
	s.cldb = &clientdb{
		//TUN
		Network: network,
		//UDP
		Port:   30000,
		Protoc: "udp4",
		//Cipher
		CipherKey:  "",
		CipherType: cipher.NONE,
		//router
		byName: make(map[string]*ClientMapElem),
		byIP:   make(map[uint32]*ClientMapElem),
	}

	var wrnTopic, regTopic, logTopic string
	if config.Static.Broker.Protocol == "amqp" {
		wrnTopic = "mtd.warnings.*"
		regTopic = "mtd.registration"
		logTopic = "mtd.logs"
	} else {
		wrnTopic = "mtd/warnings/*"
		regTopic = "mtd/registration"
		logTopic = "mtd/logs"
	}

	s.warnings = connection.NewConsumer(wrnTopic, s.handleWarnings)
	s.registration = connection.NewConsumer(regTopic, s.handleRegistrations)
	s.logs = connection.NewProducer(logTopic)
	s.logChan = make(chan string)

	s.configTimer = time.NewTicker(time.Duration(config.Static.VPN.RefreshTime) * time.Second)
	s.configTimerStop = make(chan struct{})
	s.configTriggerChan = make(chan configTrigger)
	s.iterations = 0

	return s
}

func (s *Server) Start() {

	go s.warnings.Start()

	go s.registration.Start()

	go s.logs.Start()

	go s.handleLogs()

	go s.handleTimer()

	go s.handleConfig()

	s.warnings.WaitUntilReady()
	s.registration.WaitUntilReady()
	s.logs.WaitUntilReady()

	rlog.Info(config.ClrYellowFg + "[MTD Server] Active" + config.ClrReset)
	rlog.Infof(config.ClrYellowFg+"[MTD Server] Tunnel Interface CIDR: %s"+config.ClrReset, config.Static.VPN.TunnelCIDR)
	rlog.Infof(config.ClrYellowFg+"[MTD Server] Config Refresh Time: %d Seconds"+config.ClrReset, config.Static.VPN.RefreshTime)
}

func (s *Server) Stop() {
	s.deregisterAll()

	time.Sleep(2 * time.Second)
	s.warnings.Stop(nil)
	s.registration.Stop(nil)
	s.logs.Stop(nil)

	s.logChan <- "STOP"
	s.configTimer.Stop()
	s.configTimerStop <- struct{}{}
	s.configTriggerChan <- configTrigger{stopTr, nil}
}

func (s *Server) handleLogs() {
	for msg := range s.logChan {
		if msg == "STOP" {
			return
		}

		json_msg := struct {
			Timestamp time.Time `json:"@timestamp"`
			Message   string    `json:"Message"`
		}{time.Now(), msg}

		json_msg_m, err := json.Marshal(&json_msg)
		if err != nil {
			fmt.Print(err, "[handleLogs] Failed to marshal message")
		}

		rlog.Debugf("Sending to sap: %s", string(json_msg_m))
		s.logs.GetTx() <- json_msg_m
	}
}

func (s *Server) handleConfig() {
	for trigger := range s.configTriggerChan {

		switch trigger.trType {
		case timerTr:
			rlog.Debug("[handleConfig] Generating new config from timer")
			err := s.generateConfigFromTimer()
			if err != nil {
				rlog.Warn("[manageConfig] Failed to generate new config after timer event: ", err)
			}

		case registrationTr:
			registeredClient, ok := trigger.trData.(*ClientMapElem)
			if ok {
				rlog.Info("[handleConfig] Generating new config from registration")
				err := s.generateConfigFromRegistration(registeredClient, false)
				if err != nil {
					rlog.Warn("[manageConfig] Failed to generate new config after registration: ", err)
				}
			}

		case deregistrationTr:
			deregisteredClient, ok := trigger.trData.(*ClientMapElem)
			if ok {
				rlog.Info("[handleConfig] Generating new config from deregistration")
				err := s.generateConfigFromRegistration(deregisteredClient, true)
				if err != nil {
					rlog.Warn("[manageConfig] Failed to generate new config after deregistration: ", err)
				}
			}

		case warningTr:
			delivery, ok := trigger.trData.(shared.Delivery)
			rlog.Tracef(1, "Topic: %s, Got %s warning: "+config.ClrRedFg+"%s"+config.ClrReset, delivery.GetTopic(), delivery.GetName(), delivery.GetBody())
			if ok {
				rlog.Info("[handleConfig] Generating new config from warning")
				err := s.generateConfigFromWarning(delivery)
				if err != nil {
					rlog.Warn("[manageConfig] Failed to generate new config after warning: ", err)
				}
			}

		case stopTr:
			return
		}

	}
}

type rest_msg struct {
	name   string
	ip     string
	action string
	mac    string
	key    string
}

type Nodes struct {
	DeviceID        string   `json:"device_id"`
	DeviceType      string   `json:"device_type"`
	IPAddr          string   `json:"ip_addr"`
	MacAddr         string   `json:"mac_addr"`
	EmbeddedStreams []string `json:"embedded_streams"`
	ID              string   `json:"id"`
	Isolated        bool     `json:"isolated"`
}
type Links struct {
	Port               string   `json:"port"`
	PortSpeed          int      `json:"port_speed"`
	LinkSpeed          int      `json:"link_speed"`
	LinkFabric         string   `json:"link_fabric"`
	LinkDistanceM      int      `json:"link_distance_m"`
	ReservedBandwidth  int      `json:"reserved_bandwidth"`
	AvailableBandwidth int      `json:"available_bandwidth"`
	EmbeddedStreams    []string `json:"embedded_streams"`
	Vid                []int    `json:"vid"`
	MaxSpDelay         int      `json:"max_sp_delay"`
	Source             string   `json:"source"`
	Target             string   `json:"target"`
}
type TopologyData struct {
	Nodes   []Nodes `json:"nodes"`
	Links   []Links `json:"links"`
	Version int     `json:"version"`
}
type TopologyGraph struct {
	TopologyData TopologyData `json:"topologyData"`
}

type isolate_msg struct {
	NodeID  string `json:"node_id"`
	Isolate bool   `json:"isolate"`
}

func (s *Server) updateTSNController(msg rest_msg) error {
	if s.TSN_URL == "" {
		return errors.New("TSN URL is empty")
	}

	//get the topology graph from the tsn controller
	client := resty.New()
	resp, err := client.R().
		Get(s.TSN_URL + "/topology/graph")
	if err != nil {
		return err
	}
	if resp.StatusCode() != 200 {
		return errors.New("get graph failed")
	}

	//parse the graph and search for the node by ip
	tg := TopologyGraph{}
	tg.TopologyData.Links = make([]Links, 0)
	tg.TopologyData.Nodes = make([]Nodes, 0)
	err = json.Unmarshal(resp.Body(), &tg)
	if err != nil {
		return err
	}

	for _, n := range tg.TopologyData.Nodes {
		if n.IPAddr == msg.ip {
			if n.Isolated && msg.action == "allow" {
				resp, err := client.R().
					SetBody(isolate_msg{
						NodeID:  n.ID,
						Isolate: false,
					}).
					Post(s.TSN_URL + "/action/isolate")
				if err != nil {
					return err
				}
				if resp.StatusCode() == 400 {
					return errors.New("requested node does not exist or is not discovered")
				} else if resp.StatusCode() == 500 {
					return errors.New("an internal error has occurred")
				}
			} else if !n.Isolated && msg.action == "block" {
				resp, err := client.R().
					SetBody(isolate_msg{
						NodeID:  n.ID,
						Isolate: true,
					}).
					Post(s.TSN_URL + "/action/isolate")
				if err != nil {
					return err
				}
				if resp.StatusCode() == 400 {
					return errors.New("requested node does not exist or is not discovered")
				} else if resp.StatusCode() == 500 {
					return errors.New("an internal error has occurred")
				}
			}
		}
	}

	return nil
}

//this handler manages new warnings, and triggers config updates
func (s *Server) handleWarnings(delivery shared.Delivery) error {
	s.configTriggerChan <- configTrigger{warningTr, delivery}

	return nil
}

func (s *Server) handleTimer() {
	shared.Wg.Add(1)
	defer shared.Wg.Done()

	for {
		select {
		case <-shared.Ctx.Done():
			rlog.Debug("[manageConfig] Received ctx Done, closing")
			return
		case <-s.configTimerStop:
			rlog.Debug("[manageConfig] Received Stop, closing")
			return
		case <-s.configTimer.C:
			s.configTriggerChan <- configTrigger{timerTr, nil}
		}
	}
}

//this handler manages new registrations
// On new registration with "register" action: registerClient()
// On new registration with "deregister" action: deregisterClient()
func (s *Server) handleRegistrations(delivery shared.Delivery) error {

	rlog.Debug("[registration] received request from %s", delivery.GetTopic())
	// TODO: send answer

	reg := registrationCfg{}
	err := json.Unmarshal(delivery.GetBody(), &reg)
	if err != nil {
		rlog.Error("[registration] Failed to unmarshal registration: %v", err)
		return err
	}
	rlog.Debug("[registration] received registration: %v", reg)

	if reg.Action == "register" {
		return s.registerClient(&reg)
	} else {
		return s.deregisterClient(&reg, true)
	}
}

//this function is called on a registration request and:
// A) authenticates the client that sent the registration request
// B) sends an ack to that client
// C) registers that client
// D) triggers a config update
func (s *Server) registerClient(r *registrationCfg) error {
	rlog.Info("[registration] registering client ", r.NodeName, r.NodeIP)
	rlog.Info("[registration] received request from client with MAC:", r.Mac, "and public key:", r.Key)
	//try to allocate and init new client
	c, err := NewClientMapElem(s, r)
	if err != nil {
		rlog.Error("[Registration] Failed to register client: ", err)
		return err
	}

	//try to add new client to maps
	s.cldb.RLock()
	// check both maps for conflict
	_, foundIP := s.cldb.byIP[ip2int(c.IP)]
	_, foundName := s.cldb.byName[c.Name]
	s.cldb.RUnlock()
	if foundIP {
		return errors.New("new client " + c.Name + "(" + c.IP.String() + "): client IP already in use")
	} else if foundName {
		return errors.New("new client " + c.Name + "(" + c.IP.String() + "): client name already exists")
	} else {
		// no conflicts, add client to both maps
		s.cldb.Lock()
		s.cldb.byIP[ip2int(c.IP)] = c
		s.cldb.byName[c.Name] = c
		s.cldb.Unlock()
		c.Start()
	}

	var trigger configTrigger
	trigger.trType = registrationTr
	trigger.trData = c

	s.configTriggerChan <- trigger

	sap_msg := struct {
		Source  string
		Outcome string
	}{r.NodeName + "" + r.Key + "(" + r.NodeIP + "):" + r.Action, "Success"}
	sap_json, err := json.Marshal(sap_msg)
	if err != nil {
		rlog.Error("[generateConfigFromTimer] Failed to marshal log message: ", err)
	}
	s.logChan <- string(sap_json)

	rlog.Tracef(1, config.ClrGreenFg+"[registration] registered %s (%s)"+config.ClrReset, c.Name, c.IP.String())
	return nil
}

//this function is called on a deregistration request and:
// A) sends an ack to the client that sent the deregistration request
// B) deregisters that client
// C) triggers a config update
func (s *Server) deregisterClient(r *registrationCfg, sendStop bool) error {
	rlog.Info("[deregistration] deregistering client ", r.NodeName, r.NodeIP)
	rlog.Info("[deregistration] deregistering client with MAC", r.Mac, "and public key:", r.Key)

	//try to remove client from maps
	s.cldb.Lock()
	c1, foundByName := s.cldb.byName[r.NodeName]
	c2, foundByIP := s.cldb.byIP[ip2int(c1.IP)]

	if !foundByName && !foundByIP {
		return errors.New("[deregistration] client not found")
	}

	if !foundByName && foundByIP {
		rlog.Error("[deregistration] byName - client not found")
	}
	if !foundByIP && foundByName {
		rlog.Error("[deregistration] byIP - client not found")
	}

	if c1 != c2 {
		rlog.Error("[deregistration] different clients with same info, database state is wrong")
		c2.Stop(sendStop)
	}

	c1.Stop(sendStop)
	rlog.Debug("[deregistration] stopped ", r.NodeName)

	delete(s.cldb.byName, c1.Name)
	delete(s.cldb.byIP, ip2int(c1.IP))
	s.cldb.Unlock()

	var trigger configTrigger
	trigger.trType = deregistrationTr
	trigger.trData = c1

	s.configTriggerChan <- trigger

	sap_msg := struct {
		Source  string
		Outcome string
	}{r.NodeName + "(" + r.NodeIP + "):" + r.Action, "Success"}
	sap_json, err := json.Marshal(sap_msg)
	if err != nil {
		rlog.Error("[deregistration] Failed to marshal log message: ", err)
	}
	s.logChan <- string(sap_json)

	rlog.Debug("[deregistration] deregistered ", r.NodeName)
	return nil
}

//this function is called on shutdown and:
// A) sends a shutdown message to all clients (does not wait for ack)
// B) deregisters all clients
func (s *Server) deregisterAll() {
	rlog.Info("[registration] deregistering all clients")
	s.cldb.Lock()
	// stop all amqp clients and remove them from byName map
	if len(s.cldb.byName) > 0 {
		rlog.Debug("[registration] deregistering ", len(s.cldb.byName), " (", len(s.cldb.byIP), ") clients")
		for key, c := range s.cldb.byName {
			if c != nil {
				//TODO: send shutdown message to the actual mtd client
				c.Stop(true)
				rlog.Debug("[registration] stopped ", c.Name, " (", c.IP, ")")
			}
			delete(s.cldb.byName, key)
		}
		// remove all clients from byIP map, the two maps should always have the same clients
		for key := range s.cldb.byIP {
			delete(s.cldb.byIP, key)
		}

	}
	s.cldb.Unlock()
	rlog.Debug("[registration] deregistered all clients")
}
