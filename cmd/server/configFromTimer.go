package main

import (
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"mtd/internal/config"
	"mtd/internal/vpn"
	"mtd/internal/vpn/cipher"
	"mtd/internal/vpn/router"
	"net"
	"time"

	"github.com/romana/rlog"
)

//this function generates a config with a changed property (same client list, but might change routing table)
func (s *Server) generateConfigFromTimer() error {
	a := makeTimestamp()
	rlog.Debug("[Time] Message that we need to change configuration, the beggining", a)
	s.iterations++
	//TODO: define attack surface and cfg generation algorithm
	//NOTE: for now support only changing internal network or UDP connection

	var sap_msg struct {
		Source  string
		Outcome string
		Key     string
	}
	sap_msg.Source = "Timer"
	//these are changed by hand (and recompile) for testing
	updateUDP := false // <- broken
	updateTUN := false
	updateCipher := false
	// if updateCipher is true, updateCipherKey must be true
	updateCipherKey := false || updateCipher

	/* if s.iterations%4 == 1 {
		updateUDP = true
	} else if s.iterations%4 == 2 {
		updateCipherKey = true
	} else if s.iterations%4 == 3 {
		updateTUN = true
	} else {
		updateCipher = true
		updateCipherKey = false || updateCipher
	} */
	if s.iterations%4 == 0 {
		updateUDP = true
	} else if s.iterations%4 == 1 {
		updateCipherKey = true
	} else if s.iterations%4 == 2 {
		updateUDP = true
	} else if s.iterations%4 == 3 {
		updateCipherKey = true
	}

	rlog.Info("[generateConfigFromTimer] Iterations ", s.iterations)
	s.cldb.Lock()

	ones, bits := s.cldb.Network.Mask.Size()
	ipmap := make(map[uint32]*ClientMapElem)

	newConfig := new(vpn.VPNConfig)
	newConfig.Router.RoutesToAdd = make([]router.Route, 0)
	newConfig.Router.RoutesToRemove = make([]string, 0)

	// update udp
	if updateUDP {
		s.cldb.Port++
		if s.cldb.Port > 45000 {
			s.cldb.Port = 30000
		}
		newConfig.UDP.IsSet = true
		newConfig.UDP.Port = s.cldb.Port
		newConfig.UDP.Protoc = s.cldb.Protoc
		rlog.Info("[generateConfigFromTimer] New Port ", s.cldb.Port)
	} else {
		newConfig.UDP.IsSet = false
	}

	// update tun + router
	if updateTUN {
		newConfig.TUN.IsSet = true
		newConfig.Router.IsSet = true
		// update internal (TUN) network
		s.cldb.Network.IP = int2ip(ip2int(s.cldb.Network.IP) + uint32(256))

		netmaskTmp := net.CIDRMask(ones, bits)
		for i, nm := range netmaskTmp {
			netmaskTmp[i] = ^nm
		}
		ipNum := binary.BigEndian.Uint32(netmaskTmp)
		rlog.Info("[generateConfigFromTimer] Updating internal IPs")
		//first pass, generate new internal (TUN) ips map
		for _, c := range s.cldb.byName {
			if !c.GetBlocked() {
				baseIPint := ip2int(s.cldb.Network.IP)
				//search for unused internal ip
				for i := uint32(1); i < ipNum; i++ {
					tmpIPint := baseIPint + i
					rlog.Debug("[generateConfigFromTimer] tmp IP: ", int2ip(tmpIPint))
					currClient, found := ipmap[tmpIPint]
					if !found {
						rlog.Debug("[generateConfigFromTimer] found empty slot: ", int2ip(tmpIPint))
						ipmap[tmpIPint] = c
						c.InternalIP = int2ip(tmpIPint)
						break
					} else {
						rlog.Debug("[generateConfigFromTimer] slot in use: ", int2ip(tmpIPint), ", ", currClient.Name)
					}
				}
			}
		}
	} else {
		newConfig.TUN.IsSet = false
		newConfig.Router.IsSet = false
	}

	if updateCipher {

		newCType := s.cldb.CipherType
		newCType++
		rlog.Info("[generateConfigFromTimer] New Cipher Type ", s.cldb.CipherType)
		if newCType == cipher.INVALID {
			newCType = cipher.NONE
		}
		s.cldb.CipherType = newCType
	}

	if updateCipherKey {
		var keyhash []byte
		key := []byte(cipher.RandomString(16))
		if s.cldb.CipherType == cipher.AES_CBC_HMAC || s.cldb.CipherType == cipher.AES_CBC_HMAC_Snappy {
			hash := sha256.Sum256([]byte(cipher.RandomString(16)))
			keyhash = append(key, hash[:]...)
		} else {
			keyhash = key
		}
		s.cldb.CipherKey = hex.EncodeToString(keyhash)
	}

	if updateCipher || updateCipherKey {
		newConfig.Cipher.IsSet = true
		newConfig.Cipher.CipherKey = s.cldb.CipherKey
		newConfig.Cipher.CipherType = s.cldb.CipherType
		rlog.Info("[generateConfigFromTimer] New Conf ", s.cldb.CipherKey, s.cldb.CipherType)

	} else {
		newConfig.Cipher.IsSet = false
	}

	s.cldb.Unlock()
	s.cldb.RLock()
	defer s.cldb.RUnlock()

	if updateTUN {
		//second pass, create routing table
		for _, c := range s.cldb.byName {
			if !c.GetBlocked() {
				newConfig.Router.RoutesToAdd = append(newConfig.Router.RoutesToAdd, router.Route{
					NodeName:   c.Name,
					LocalIP:    c.InternalIP.String(),
					ExternalIP: c.IP.String(),
				})
			}
		}
		rlog.Debug("[generateConfigFromTimer] Routing table ", newConfig.Router.RoutesToAdd)

	}

	//third pass, create personalized configs and send to clients
	for _, c := range s.cldb.byName {
		if !c.GetBlocked() {
			if updateTUN {
				newConfig.TUN.CIDR = c.InternalIP.String() + "/" + fmt.Sprint(ones)
				newConfig.Router.ExtIP = c.IP.String()
			}

			newConfigByte, err := json.Marshal(&newConfig)
			if err != nil {
				return fmt.Errorf("failed to marshal config: %v", err)
			}
			c.config.GetTx() <- newConfigByte
			b := makeTimestamp()
			rlog.Debug("[Time] Message when we send the configuration through the trusted channel", b)
			dif := b - a
			rlog.Debug("[Time] Time to generate the new conf", dif, "us")
		}
	}

	sap_msg.Outcome = "New Configuration"
	sap_json, err := json.Marshal(sap_msg)
	if err != nil {
		rlog.Error("[generateConfigFromTimer] Failed to marshal log message: ", err)
	}
	s.logChan <- string(sap_json)

	s.configTimer.Reset(time.Duration(config.Static.VPN.RefreshTime) * time.Second)
	return nil
}

func makeTimestamp() int64 {
	return time.Now().UnixNano() / int64(time.Microsecond)
}
