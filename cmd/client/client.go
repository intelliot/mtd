package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"net"
	"os"
	"time"

	"mtd/internal/config"
	"mtd/internal/connection"
	"mtd/internal/connection/shared"
	"mtd/internal/util"
	"mtd/internal/vpn"

	"github.com/romana/rlog"
)

type registrationCfg struct {
	Action   string
	NodeName string
	NodeIP   string
	Mac      string
	Key      string
}

type Client struct {
	NodeName     string
	NodeIP       net.IP
	Mac          string
	Key          string
	registration *connection.Producer
	config       *connection.Consumer
	kaReq        *connection.Consumer
	kaResp       *connection.Producer
	kaTimer      *time.Ticker
}

func makeTimestamp() int64 {
	return time.Now().UnixNano() / int64(time.Microsecond)
}

func (c *Client) handleNewConfig(delivery shared.Delivery) error {
	f := makeTimestamp()
	rlog.Debug("[Time] New config arrived", f)
	config := &vpn.VPNConfig{}
	err := json.Unmarshal(delivery.GetBody(), &config)
	rlog.Info("[Config] We applied new config", *config)
	if err != nil {
		return fmt.Errorf("failed to unmarshal new config: %v", err)
	}
	rlog.Debug("[config] ", delivery.GetTopic(), " -> ", *config)

	err = vpn.Update(config)
	if err != nil {
		return fmt.Errorf("failed to update config: %v", err)
	}
	g := makeTimestamp()
	rlog.Debug("[Time] New config applied", g, "ns\n")
	apply_t := g - f
	rlog.Debug("[Time] New config time to apply", apply_t, "us")
	return nil

}

func (c *Client) handleNewKAReq(delivery shared.Delivery) error {
	c.kaTimer.Reset(15 * time.Second)

	req := new(shared.KAReq)
	err := json.Unmarshal(delivery.GetBody(), &req)
	if err != nil {
		return fmt.Errorf("failed to unmarshal new trust map: %v", err)
	}
	rlog.Debugf("Client %s Req: %+v", c.NodeName, req, c.Key)
	//here other status codes can be added to send to the server
	resp := shared.KAResp{
		ID:     req.ID,
		Status: "OK",
		Key:    c.Key,
	}

	rlog.Debugf("Client %s Resp: %+v", c.NodeName, resp)
	rb, err := json.Marshal(&resp)
	if err != nil {
		rlog.Error("[handleNewKAReq] Failed to marshal KAResp")
		return errors.New("failed to marshal KAResp")
	}

	c.kaResp.GetTx() <- rb
	return nil
}

func (c *Client) handleKATimeOut() {

	shared.Wg.Add(1)
	defer shared.Wg.Done()

	for {
		select {
		case <-shared.Ctx.Done():
			rlog.Debug("[keepalive] manageKeepAlive Received ctx Done, closing")
			return
		case <-c.kaTimer.C:
			rlog.Critical("[keepalive] Server keepalive failed, Exiting")
			os.Exit(1)
		}
	}

}

func (c *Client) register() {
	rlog.Debug("[registration] started registration connection, generating request with node name ", c.NodeName)

	reg := registrationCfg{
		Action:   "register",
		NodeName: c.NodeName,
		NodeIP:   c.NodeIP.String(),
		Mac:      c.Mac,
		Key:      config.Static.Broker.GetPKey(),
	}

	rb, err := json.Marshal(&reg)
	if err != nil {
		rlog.Critical("[registration] Failed to marshal registration")
		os.Exit(1)
	}
	rlog.Debug("[registration] marshaled Registration: ", string(rb))

	rlog.Debug("[registration] sending registration request")
	c.registration.GetTx() <- rb
	rlog.Debug("[registration] sent registration request")
}

func (c *Client) deregister() {
	rlog.Debug("[deregistration] started deregistration connection, generating request with node name ", c.NodeName)

	reg := registrationCfg{
		Action:   "deregister",
		NodeName: c.NodeName,
		NodeIP:   "",
		Mac:      c.Mac,
		Key:      config.Static.Broker.GetPKey(),
	}

	rb, err := json.Marshal(&reg)
	if err != nil {
		rlog.Critical("[deregistration] Failed to marshal deregistration: ", err)
		os.Exit(1)
	}
	rlog.Debug("[deregistration] marshaled Deregistration: ", string(rb))

	rlog.Debug("[deregistration] sending deregistration request")
	c.registration.GetTx() <- rb
	rlog.Debug("[deregistration] sent deregistration request")
}

func NewClient(nodeName string, nodeIP net.IP, Mac string) *Client {
	if nodeIP == nil {
		rlog.Critical("[registration] node IP is invalid ")
		os.Exit(1)
	}

	c := new(Client)
	c.NodeName = nodeName
	c.NodeIP = nodeIP
	c.Mac = Mac
	c.Key = config.Static.Broker.GetPKey()

	var regTopic, confTopic, reqTopic, respTopic string
	if config.Static.Broker.Protocol == "amqp" {
		regTopic = "mtd.registration"
		confTopic = "mtd.config." + nodeName
		reqTopic = "mtd.keepaliveReq." + nodeName
		respTopic = "mtd.keepaliveResp." + nodeName
	} else {
		regTopic = "mtd/registration"
		confTopic = "mtd/config/" + nodeName
		reqTopic = "mtd/keepaliveReq/" + nodeName
		respTopic = "mtd/keepaliveResp/" + nodeName
	}

	c.registration = connection.NewProducer(regTopic)
	c.config = connection.NewConsumer(confTopic, c.handleNewConfig)

	c.kaReq = connection.NewConsumer(reqTopic, c.handleNewKAReq)
	c.kaResp = connection.NewProducer(respTopic)

	c.kaTimer = time.NewTicker(10 * time.Second)

	return c
}

func (c *Client) Start() {

	rlog.Debug("[registration] starting registration connection to ", c.registration.GetTopic())

	go c.registration.Start()

	rlog.Debug("[config] starting config connection to ", c.config.GetTopic())

	go c.config.Start()

	rlog.Debug("[keepalive] starting keepalive consumer connection to ", c.kaReq.GetTopic())

	go c.kaReq.Start()

	rlog.Debug("[keepalive] starting keepalive producer connection to ", c.kaResp.GetTopic())

	go c.kaResp.Start()

	go c.handleKATimeOut()

	c.config.WaitUntilReady()
	c.kaReq.WaitUntilReady()
	c.kaResp.WaitUntilReady()
	c.registration.WaitUntilReady()
	c.register()

}

func (c *Client) Stop() {
	c.deregister()

	time.Sleep(2 * time.Second)
	c.registration.Stop(nil)
	c.config.Stop(nil)
	c.kaReq.Stop(nil)
	c.kaResp.Stop(nil)

	c.kaTimer.Stop()
}

var args struct {
	configPath *string
	ctag       *string
}

func init() {
	args.configPath = flag.String("cfg", "./client_config.json", "The path to the configuration file")
	flag.Parse()
	config.LoadConfiguration(*args.configPath)
}

func main() {
	err := vpn.Start()
	if err != nil {
		rlog.Criticalf("Unable to start vpn: %s", err)
		os.Exit(1)
	}
	c := NewClient(connection.GetName(), vpn.GetIP(), vpn.GetMAC())
	c.Start()
	defer func() {
		c.Stop()
		shared.CloseAll()
	}()

	util.WaitForInterrupt()
}
