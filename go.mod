module mtd

go 1.17

require (
	github.com/eclipse/paho.golang v0.10.0
	github.com/go-resty/resty/v2 v2.7.0
	github.com/golang/snappy v0.0.4
	github.com/romana/rlog v0.0.0-20171115192701-f018bc92e7d7
	github.com/songgao/water v0.0.0-20200317203138-2b4b6d7c09d8
	github.com/streadway/amqp v1.0.0
	github.com/vishvananda/netlink v1.1.0
	golang.org/x/net v0.0.0-20220111093109-d55c255bac03
)

require (
	github.com/vishvananda/netns v0.0.0-20191106174202-0a2b9b5464df // indirect
	golang.org/x/sync v0.0.0-20201207232520-09787c993a3a // indirect
	golang.org/x/sys v0.0.0-20210423082822-04245dca01da // indirect
)
