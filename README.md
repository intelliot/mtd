# How to make the binaries to test it

1) Inside the Makefile we choose the architecture and the platform that we want

ARCHITECTURE := arm64
PLATFORM := debian

2) make cross

3) cd bin/

4) Inside there you will find the binaries of the Mtd-Server and the Mtd-Clients


# Full Demo

To find information on how to test the MTD Server and Clients you can use the Intelliot Final Demo Repo
https://github.com/parasecurity/intelliot_final_review/tree/binaries

In the branch of binaries you can add yours and test it.

# MTD Client

MTD Client is part of the IntellIoT security compononents. Security
components communicate over a secure channel provided by a common AMQP
broker.

## Registration topic

- mtd.registration
- write access

Every time an MTD Client starts, it needs to register to the MTD Server by
publishing in this topic. It can also publish a deregister action for a
graceful removal from the MTD Server.

Example payload:

```
{
  "Action": "register",
  "NodeName": "node name",
  "NodeIp": "node ip"
}
```

```
{
  "Action": "deregister",
  "NodeName": "node name",
  "NodeIp": ""
}
```

## Configuration topic

- mtd.config.<node>
- read access

Each MTD Client subscribes to this topic in order to get new configurations
from the MTD Server.

Example payload:

```
{
  "CIDR": "10.0.0.2/24",
  "Port": 30000,
  "Protoc": "udp4",
  "CipherKey": "3863766a4c4f553166502d716a324b69",
  "CipherType": 3,
  "ExtIP": "192.168.176.5",
  "LocIP": "10.0.0.2",
  "RoutesToAdd": {
    "10.0.0.1": "192.168.176.4",
    "10.0.0.2": "192.168.176.5",
  },
  "RoutesToRemove": [
    "10.0.0.3"
  ]
}
```

## Keepalive Request topic

- mtd.keepaliveReq.<node>
- read access

Each MTD Client listens to this topic for keepalive requests from the MTD
Server. The payload is a random string.

## Keepalive Response topic

- mtd.keepaliveResp.<node>
- write access

Each MTD Client publishes to this topic its keepalive response to the MTD
Server. The payload is the random string it received in the request appended
with an " OK".





# MTD Server

MTD Server is part of the IntellIoT security compononents. Security
components communicate over a secure channel provided by a common AMQP
broker.

## Registration topic

- mtd.registration
- read access

MTD Server listens to this topic for new MTD Client instances registration
and deregistration actions.

Example payload:

```
{
  "Action": "register",
  "NodeName": "node name",
  "NodeIp": "node ip"
}
```

```
{
  "Action": "deregister",
  "NodeName": "node name",
  "NodeIp": ""
}
```

## Configuration topic

- mtd.config.<node>
- write access

MTD Server publishes to this topic new configurations for each registered
MTD Client. This can happen either periodically or by trigger.

Example payload:

```
{
  "CIDR": "10.0.0.2/24",
  "Port": 30000,
  "Protoc": "udp4",
  "CipherKey": "3863766a4c4f553166502d716a324b69",
  "CipherType": 3,
  "ExtIP": "192.168.176.5",
  "LocIP": "10.0.0.2",
  "RoutesToAdd": {
    "10.0.0.1": "192.168.176.4",
    "10.0.0.2": "192.168.176.5",
  },
  "RoutesToRemove": [
    "10.0.0.3"
  ]
}
```

## Keepalive Request topic

- mtd.keepaliveReq.<node>
- write access

MTD Server publishes periodically to this topic a keepalive request for each
registered MTD Client. The payload is a random string.

## Keepalive Response topic

- mtd.keepaliveResp.<node>
- read access

MTD Server listens to this topic for keepalive responses from the registered
MTD Clients. The expected payload is the random string sent to an MTD Client
appended by " OK". If an MTD Client fails to respond in a timely manner it
is automatically deregistered.

## IDS Trust topic

- ids.trust.<node>
- read access

MTD Server listens to this topic to collect all IDS instances trust values.

Example payload:

```
{
  "node0": 0.0,
  "node1": 1.0,
}
```

## IDS Warn topic

- ids.warn.<node>
- read access

MTD Server listens to this topic to collect all IDS instances warnings.

Example payload:

```
{
  "nodes": [
    "node0",
    "node1"
  ]
}
```

## Alert topic

- mtd.alert
- write access

MTD Server decides for legitimate IDS warnings and raises alerts for
Security Assurance Platform.

Example payload: TBD

## Trigger topic

- mtd.trigger
- read access

MTD Server listens to this topic in order to trigger a configuration change
as a response to a threat identified by the Security Assurance Platform.

Example payload: TBD
