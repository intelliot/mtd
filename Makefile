# The name of the executable
TARGET := $(shell awk '/^module/{print $$2}' go.mod)
TARGET_CLIENT := bin/$(TARGET)-client
TARGET_SERVER := bin/$(TARGET)-server
COMMAND_CLIENT := cmd/client/*.go
COMMAND_SERVER := cmd/server/*.go

# Cross Compilation
#
# Usually this is easy to be done. In this case we have dependencies
# to libpcap-dev which should be natively build. This is much easier
# to do by using a proper docker image with the library natively installed
# and ids build inside it.
#
# Two helper scripts are provided:
#
# - builder.sh, that creates the proper image
# - crosscompile.sh, that runs make inside the proper image
#
# Supported platforms are:
#
# - debian (libc based)
# - alpine (musl based)
#
# Supported architectures for debian platform:
#
# - 386
# - amd64
# - arm/v5
# - arm/v7
# - arm64/v8
# - mips64le
# - ppc64le
# - s390x
#
# Supported architectures for alpine platform:
# - 386
# - amd64
# - arm/v6
# - arm/v7
# - arm64/v8
# - ppc64le
# - s390x
ARCHITECTURE := arm64
PLATFORM := debian

# go source files, ignore vendor directory
SRC = $(shell find . -type f -name '*.go' -not -path "./vendor/*")

all: build

$(TARGET_CLIENT): $(SRC)
	go build -o $(TARGET_CLIENT) $(COMMAND_CLIENT)

$(TARGET_SERVER): $(SRC)
	go build -o $(TARGET_SERVER) $(COMMAND_SERVER)

build: $(TARGET_CLIENT) $(TARGET_SERVER)

cross:
	@./scripts/builder.sh $(ARCHITECTURE) $(PLATFORM)
	@./scripts/crosscompile.sh $(ARCHITECTURE) $(PLATFORM)

clean:
	rm -f $(TARGET_CLIENT) $(TARGET_SERVER)

.PHONY: fmt simplify vet build setcap clean
