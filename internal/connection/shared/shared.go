package shared

import (
	"context"
	"math/rand"
	"strconv"
	"sync"
)

type Delivery interface {
	GetTopic() string
	GetBody() []byte
	GetName() string
}

type KAReq struct {
	ID string
	//add any other info from server to client
}

type KAResp struct {
	ID     string
	Status string
	Key    string
	//add any other status info from client to server
}

func GetCorrelationId() string {
	return strconv.Itoa(rand.Intn(9999999999))
}

var (
	Wg         *sync.WaitGroup
	Ctx        context.Context
	CancelFunc context.CancelFunc
)

func init() {
	Wg = &sync.WaitGroup{}
	Ctx, CancelFunc = context.WithCancel(context.Background())
}

func CloseAll() {
	CancelFunc()
	Wg.Wait()
}
