package connection

import (
	"mtd/internal/connection/shared"
	"sync"
	"time"

	"github.com/romana/rlog"
)

type Consumer struct {
	conn     Connection
	done     chan error
	running  bool
	topic    string
	callback CallbackFunc
	lock     sync.RWMutex
}

type CallbackFunc func(delivery shared.Delivery) error

func NewConsumer(topic string, callback CallbackFunc) *Consumer {
	rlog.Debug("[Consumer ", topic, "] Opening with topic ", topic)

	ptr := new(Consumer)
	ptr.conn = nil
	ptr.done = make(chan error)
	ptr.running = false
	ptr.topic = topic
	ptr.callback = callback

	rlog.Debug("[Consumer ", topic, "] Opened with topic ", topic)
	return ptr
}

func (c *Consumer) Start() {
	var err error
	c.lock.Lock()

	shared.Wg.Add(1)

	rlog.Debug("[Consumer ", c.topic, "] Starting new connection")
	c.conn, err = NewConnection()
	if err != nil {
		rlog.Warn("[Consumer ", c.topic, "] Failed to start connection: ", err)
		return
	}

	rlog.Debug("[Consumer ", c.topic, "] Subscribing to ", c.topic)
	queue, err := c.conn.Subscribe(c.topic)
	if err != nil {
		rlog.Warn("[Consumer ", c.topic, "] Failed to subscribe: ", err)
		return
	}

	c.running = true

	defer func() {
		c.lock.Lock()
		c.running = false
		c.conn.Unsubscribe(c.topic)
		err = c.conn.Close()
		c.conn = nil
		c.lock.Unlock()
		shared.Wg.Done()
	}()
	c.lock.Unlock()

	for {
		select {
		case <-shared.Ctx.Done():
			rlog.Debug("[Consumer ", c.topic, "] Received ctx Done, closing")
			if err != nil {
				rlog.Warn("[Consumer ", c.topic, "] Failed to close: ", err)
			}
			return

		case <-c.done:
			rlog.Debug("[Consumer ", c.topic, "] Received Done, closing")
			if err != nil {
				rlog.Warn("[Consumer ", c.topic, "] Failed to close: ", err)
			}
			return

		case delivery, ok := <-queue:
			if !ok {
				rlog.Debug("[Consumer ", c.topic, "] RxChan closed, closing")
				if err != nil {
					rlog.Warn("[Consumer ", c.topic, "] Failed to close: ", err)
				}
				return
			}
			rlog.Debug("[Consumer ", c.topic, "] New Delivery with topic ", delivery.GetTopic())
			err = c.callback(delivery)
			if err != nil {
				rlog.Warn("[Consumer ", c.topic, "] Callback function error: ", err)
			}
		}
	}
}

func (c *Consumer) Stop(err error) {
	c.lock.RLock()
	if c.running {
		rlog.Debug("[Consumer ", c.topic, "] Sending Done")
		c.done <- err
		rlog.Debug("[Consumer ", c.topic, "] Sent Done")
	}
	c.lock.RUnlock()
}

func (c *Consumer) WaitUntilReady() {
	running := false
	for !running {
		c.lock.RLock()
		running = c.running
		c.lock.RUnlock()
		time.Sleep(10 * time.Millisecond)
	}
}

func (c *Consumer) GetReady() bool {
	return c.running
}

func (c *Consumer) GetTopic() string {
	return c.topic
}
