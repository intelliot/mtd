package connection

import (
	"mtd/internal/connection/shared"
	"sync"
	"time"

	"github.com/romana/rlog"
)

// type Producer interface {
// 	Start()
// 	Stop(err error)
// 	WaitUntilReady()

// 	GetReady() bool
// 	GetTx() chan []byte
// 	GetTopic() string
// }

type Producer struct {
	conn    Connection
	txChan  chan []byte
	done    chan error
	running bool
	topic   string
	lock    sync.RWMutex
}

func NewProducer(topic string) *Producer {
	rlog.Debug("[Producer ", topic, "] Opening with topic ", topic)

	ptr := new(Producer)
	ptr.conn = nil
	ptr.done = make(chan error)
	ptr.running = false
	ptr.txChan = make(chan []byte)
	ptr.topic = topic

	rlog.Debug("[Producer ", topic, "] Opened with topic ", topic)

	return ptr
}

func (p *Producer) Start() {
	var err error
	p.lock.Lock()

	shared.Wg.Add(1)

	if p.conn == nil {
		rlog.Debug("[Producer ", p.topic, "] Starting new connection")
		p.conn, err = NewConnection()
		if err != nil {
			if err != nil {
				rlog.Warn("[Producer ", p.topic, "] Failed to start connection: ", err)
			}
			return
		}
	}

	defer func() {
		p.lock.Lock()
		p.running = false

		err = p.conn.Close()
		p.conn = nil
		p.lock.Unlock()

		shared.Wg.Done()
	}()

	p.running = true

	p.lock.Unlock()

	for {
		select {
		case <-shared.Ctx.Done():
			rlog.Debug("[Producer ", p.topic, "] Received ctx Done, closing")
			if err != nil {
				rlog.Warn("[Producer ", p.topic, "] Failed to close: ", err)
			}
			return
		case <-p.done:
			rlog.Debug("[Producer ", p.topic, "] Received Done, closing")
			if err != nil {
				rlog.Warn("[Producer ", p.topic, "] Failed to close: ", err)
			}
			return
		case body, ok := <-p.txChan:
			if !ok {
				rlog.Debug("[Producer ", p.topic, "] TxChan closed, closing")
				if err != nil {
					rlog.Warn("[Producer ", p.topic, "] Failed to close: ", err)
				}
				return
			}
			rlog.Debug("[Producer ", p.topic, "] Sending Message from TxChan to topic ", p.topic)
			p.conn.Publish("mtd-server-to-sap", p.topic, body)
		}
	}
}

func (p *Producer) Stop(err error) {
	p.lock.RLock()
	if p.running {
		rlog.Debug("[Producer ", p.topic, "] Sending Done")
		p.done <- err
		rlog.Debug("[Producer ", p.topic, "] Sent Done")
	}
	p.lock.RUnlock()
}

func (p *Producer) WaitUntilReady() {
	running := false
	for !running {
		p.lock.RLock()
		running = p.running
		p.lock.RUnlock()
		time.Sleep(10 * time.Millisecond)
	}
}

func (p *Producer) GetReady() bool {
	return p.running
}

func (p *Producer) GetTx() chan []byte {
	return p.txChan
}

func (p *Producer) GetTopic() string {
	return p.topic
}
