package amqp

import (
	"errors"
	"os"
	"strings"

	"mtd/internal/config"
	"mtd/internal/connection/shared"
	"mtd/internal/util"

	"github.com/romana/rlog"
	_amqp "github.com/streadway/amqp"
)

type _AMQPMapElem struct {
	q    _amqp.Queue
	cin  <-chan _amqp.Delivery
	cout chan shared.Delivery
}

type _AMQPConnection struct {
	amqp_cfg _amqp.Config
	conn     *_amqp.Connection
	channel  *_amqp.Channel
	subMap   util.Map
	pqok     bool
}

type AMQPDelivery struct {
	*_amqp.Delivery
}

func (d AMQPDelivery) GetTopic() string {
	return d.RoutingKey
}

func (d AMQPDelivery) GetBody() []byte {
	return d.Body
}

func (d AMQPDelivery) GetName() string {
	key := d.RoutingKey
	keys := strings.Split(key, ".")
	return keys[len(keys)-1]
}

func NewAMQPConnection() (*_AMQPConnection, error) {
	var brokerCfg *config.BrokerConfig = &config.Static.Broker

	var err error
	cn := new(_AMQPConnection)
	cn.amqp_cfg.TLSClientConfig = brokerCfg.GetTls()

	cn.conn, err = _amqp.DialConfig(brokerCfg.GetUrl(), cn.amqp_cfg)
	if err != nil {
		return nil, errors.New("AMQP:Connection - Failed to open connection - " + err.Error())
	}
	cn.channel, err = cn.conn.Channel()
	if err != nil {
		cn.conn.Close()
		cn.conn = nil
		return nil, errors.New("AMQP:Connection - Failed to open channel - " + err.Error())
	}

	err = cn.channel.Confirm(false)
	if err != nil {
		return nil, errors.New("AMQP:Connection - Failed to set channel into confirm mode - " + err.Error())
	}

	cn.subMap = util.NewThreadSafeMap()
	cn.pqok = false

	rlog.Debug("[AMQPConnection] Connected to ", brokerCfg.GetUrl())
	return cn, nil
}

func (cn *_AMQPConnection) Close() error {
	var brokerCfg *config.BrokerConfig = &config.Static.Broker

	err := cn.channel.Close()
	if err != nil {
		return errors.New("AMQP:Connection - Failed to close channel - " + err.Error())
	}
	cn.channel = nil

	err = cn.conn.Close()
	if err != nil {
		return errors.New("AMQP:Connection - Failed to close connection - " + err.Error())
	}
	cn.conn = nil

	rlog.Debug("[AMQPConnection] Disconnected from ", brokerCfg.GetUrl())
	return nil
}

func (cn *_AMQPConnection) Subscribe(key string) (<-chan shared.Delivery, error) {
	var brokerCfg *config.BrokerConfig = &config.Static.Broker

	err := cn.channel.ExchangeDeclare(
		"amq.topic", // name
		"topic",     // type
		true,        // durable
		false,       // auto-deleted
		false,       // internal
		false,       // no-wait
		nil,         // arguments
	)
	if err != nil {
		return nil, errors.New("AMQP:Connection - Failed to declare an exchange - " + err.Error())
	}

	queue, err := cn.channel.QueueDeclare(
		"",    // name of the queue
		false, // durable
		false, // delete when unused
		false, // exclusive
		false, // noWait
		nil,   // arguments
	)
	if err != nil {
		return nil, errors.New("AMQP:Connection - Failed to declare a queue - " + err.Error())
	}

	err = cn.channel.QueueBind(
		queue.Name,  // name of the queue
		key,         // bindingKey
		"amq.topic", // sourceExchange
		false,       // noWait
		nil,         // arguments
	)
	if err != nil {
		return nil, errors.New("AMQP:Connection - Failed to bind the queue - " + err.Error())
	}

	rlog.Debug("[AMQP:Connection] Queue ", queue.Name, " bound to Exchange amq.topic with key ", key)

	cin, err := cn.channel.Consume(
		queue.Name,         // name
		brokerCfg.ClientID, // consumerTag,
		false,              // noAck
		true,               // exclusive
		false,              // noLocal
		false,              // noWait
		nil,                // arguments
	)
	if err != nil {
		return nil, errors.New("AMQP:Connection - Failed to open a channel to the queue bound to key " + key + " - " + err.Error())
	}

	cout := make(chan shared.Delivery)
	shared.Wg.Add(1)
	go cn.convertChannel(cin, cout)

	elem := _AMQPMapElem{}
	elem.q = queue
	elem.cin = cin
	elem.cout = cout
	err = cn.subMap.Add(key, elem)

	if err != nil {
		return cout, errors.New("AMQP:Connection - Failed to add queue to map, orphan queue " + queue.Name + " for key " + key + " - " + err.Error())
	}

	rlog.Debug("[AMQP:Connection] Subscribed to ", key)
	return cout, nil
}

func (cn *_AMQPConnection) Unsubscribe(key string) error {
	elem, found := cn.subMap.Get(key)
	if !found {
		return errors.New("AMQP:Connection - Subscription with key " + key + " not found")
	}
	_, err := cn.channel.QueueDelete(elem.(_AMQPMapElem).q.Name, false, false, false)
	if err != nil {
		return errors.New("AMQP:Connection - Failed to delete the queue bound to key " + key + " - " + err.Error())
	}

	rlog.Debug("[AMQP:Connection] Unubscribed from ", key)
	return nil
}

func (cn *_AMQPConnection) convertChannel(in <-chan _amqp.Delivery, out chan<- shared.Delivery) {
loop:
	for {
		select {
		case del, ok := <-in:
			if !ok {
				rlog.Debug("[AMQP:Connection] convertChannel Input channel closed, exiting")
				break loop
			}
			out <- AMQPDelivery{&del}
		case <-shared.Ctx.Done():
			rlog.Debug("[AMQP:Connection] convertChannel Received Context Done, exiting")
			break loop
		}
	}
	close(out)
	shared.Wg.Done()
}

func (cn *_AMQPConnection) Publish(q string, key string, data []byte) error {

	if !cn.pqok {
		cn.initPQ(q, key)
	}

	err := cn.channel.Publish(
		"amq.topic", // exchange
		key,         // routing key
		false,       // mandatory
		false,       // immediate
		_amqp.Publishing{
			ContentType: "text/plain",
			Body:        data,
		},
	)
	if err != nil {
		return errors.New("AMQP:Connection - Failed to publish message to key " + key + " - " + err.Error())
	}

	rlog.Debug("[AMQP:Connection] sent Message from TxChan to topic ", key)
	return nil
}

func (cn *_AMQPConnection) initPQ(q string, key string) {
	foo, err := cn.channel.QueueDeclare(
		q,     // name of the queue
		false, // durable
		false, // delete when unused
		false, // exclusive
		false, // noWait
		nil,   // arguments
	)
	if err != nil {
		rlog.Critical(err)
		os.Exit(1)
	}

	err = cn.channel.QueueBind(
		foo.Name,    // name of the queue
		key,         // bindingKey
		"amq.topic", // sourceExchange
		false,       // noWait
		nil,         // arguments
	)
	if err != nil {
		rlog.Critical(err)
		os.Exit(1)
	}

	cn.pqok = true
}
