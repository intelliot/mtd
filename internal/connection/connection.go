package connection

import (
	"mtd/internal/config"
	"mtd/internal/connection/amqp"
	"mtd/internal/connection/mqtt"
	"mtd/internal/connection/shared"
)

type Connection interface {
	Close() error
	Subscribe(key string) (<-chan shared.Delivery, error)
	Unsubscribe(key string) error
	Publish(q string, key string, data []byte) error
}

func NewConnection() (Connection, error) {
	if config.Static.Broker.Protocol == "amqp" {
		return amqp.NewAMQPConnection()
	} else {
		return mqtt.NewMQTT5Connection()
	}
}

func GetName() string {
	return config.Static.Broker.User + "_" + config.Static.Broker.ClientID
}
