package mqtt

import (
	_tls "crypto/tls"
	"errors"
	"fmt"
	"mtd/internal/config"
	"mtd/internal/connection/shared"
	"mtd/internal/util"
	"net"
	"strings"
	"sync"

	"github.com/eclipse/paho.golang/paho"
	"github.com/romana/rlog"
)

var (
	conn _MQTT5ConnectionSingleton
)

type _MQTT5MapElem struct {
	cin  chan *paho.Publish
	cout chan shared.Delivery
}

type _MQTT5ConnectionSingleton struct {
	client *paho.Client
	conn   *paho.Connect
	subMap util.Map
	lock   sync.RWMutex
	users  int
}

type _MQTT5Connection struct {
	p *_MQTT5ConnectionSingleton
}

type MQTT5Delivery struct {
	*paho.Publish
}

func (d MQTT5Delivery) GetTopic() string {
	return d.Topic
}

func (d MQTT5Delivery) GetBody() []byte {
	return d.Payload
}

func (d MQTT5Delivery) GetName() string {
	key := d.Topic
	keys := strings.Split(key, "/")
	return keys[len(keys)-1]
}

func NewMQTT5Connection() (*_MQTT5Connection, error) {
	var brokerCfg *config.BrokerConfig = &config.Static.Broker

	rlog.Debug("[MQTT5:Connection] Connecting to ", brokerCfg.GetUrl())
	conn.lock.Lock()
	defer conn.lock.Unlock()
	var err error
	if conn.conn == nil {
		rlog.Debug("[MQTT5:Connection] Openning new connection")
		var Conn net.Conn
		if brokerCfg.TLS.Enabled {

			Conn, err = _tls.Dial("tcp", brokerCfg.GetUrl(), brokerCfg.GetTls())
		} else {
			rlog.Debug("[MQTT5:Connection] Trying to connect to ", brokerCfg.GetUrl())
			Conn, err = net.Dial("tcp", brokerCfg.GetUrl())
		}
		if err != nil {
			return nil, errors.New("MQTT5:Connection - Failed to open connection - " + err.Error())
		}

		conn.client = paho.NewClient(paho.ClientConfig{
			ClientID: brokerCfg.ClientID,
			Conn:     Conn,
			Router: paho.NewSingleHandlerRouter(func(m *paho.Publish) {
				data, _ := conn.subMap.GetAll()
				for elem := range data {
					if elem != nil {
						elem.(_MQTT5MapElem).cin <- m
					}
				}
			}),
		})

		conn.conn = &paho.Connect{
			KeepAlive:  30,
			ClientID:   brokerCfg.ClientID, // + "_" + uuid.NewString(),
			CleanStart: true,
			Username:   brokerCfg.User,
			Password:   []byte(brokerCfg.Password),
		}
		if brokerCfg.User != "" {
			conn.conn.UsernameFlag = true
		}
		if brokerCfg.Password != "" {
			conn.conn.PasswordFlag = true
		}

		if conn.subMap == nil {
			conn.subMap = util.NewThreadSafeMap()
		}

		ca, err := conn.client.Connect(shared.Ctx, conn.conn)
		if ca != nil && ca.ReasonCode != 0 {
			return nil, errors.New("MQTT5:Connection - Failed to open connection to " + brokerCfg.GetUrl() + " : " + fmt.Sprintf("%x", ca.ReasonCode) + " - " + ca.Properties.ReasonString)
		} else if err != nil {
			return nil, errors.New("MQTT5:Connection - Failed to open connection - " + err.Error())
		}

		conn.users = 1
		rlog.Debug("[MQTT5:Connection] Connected to ", brokerCfg.GetUrl())
	} else {
		conn.users++
		rlog.Debug("[MQTT5:Connection] Adding user to open connection to ", brokerCfg.GetUrl())
	}

	cn := new(_MQTT5Connection)
	cn.p = &conn
	rlog.Debug("[MQTT5:Connection] Added user to open connection to ", brokerCfg.GetUrl())

	return cn, nil
}

func (cn *_MQTT5Connection) Close() error {
	var brokerCfg *config.BrokerConfig = &config.Static.Broker

	rlog.Debug("[MQTT5:Connection] Disconnecting from ", brokerCfg.GetUrl())
	cn.p.lock.Lock()
	defer cn.p.lock.Unlock()
	cn.p.users--
	if cn.p.users == 0 {
		da := &paho.Disconnect{ReasonCode: 0}
		err := cn.p.client.Disconnect(da)
		if da.ReasonCode != 0 {
			return errors.New("MQTT5:Connection - Failed to close connection to " + brokerCfg.GetUrl() + " : " + fmt.Sprintf("%x", da.ReasonCode) + " - " + da.Properties.ReasonString)
		} else if err != nil {
			return errors.New("MQTT5:Connection - Failed to close connection - " + err.Error())
		}
		rlog.Debug("[MQTT5:Connection] Disconnected from ", brokerCfg.GetUrl())
		cn.p.conn = nil
		cn.p.subMap.DeleteAll()
	}
	return nil
}

func (cn *_MQTT5Connection) Subscribe(key string) (<-chan shared.Delivery, error) {
	var brokerCfg *config.BrokerConfig = &config.Static.Broker

	rlog.Debug("[MQTT5:Connection] Subscribing to ", key)
	cn.p.lock.RLock()
	defer cn.p.lock.RUnlock()
	sa, err := cn.p.client.Subscribe(shared.Ctx, &paho.Subscribe{
		Subscriptions: map[string]paho.SubscribeOptions{
			key: {
				QoS: byte(brokerCfg.MQTT_QoS),
			},
		},
	})
	if err != nil {
		return nil, errors.New("MQTT5:Connection - Failed to subscribe to " + key + " - " + err.Error())
	} else if sa.Reasons[0] != byte(brokerCfg.MQTT_QoS) {
		return nil, errors.New("MQTT5:Connection - Failed to subscribe to " + key + " : " + fmt.Sprintf("%x", sa.Reasons[0]) + " - " + sa.Properties.ReasonString)
	}

	cin := make(chan *paho.Publish)
	cout := make(chan shared.Delivery)
	shared.Wg.Add(1)
	go cn.convertChannel(cin, cout, key)

	elem := _MQTT5MapElem{}
	elem.cin = cin
	elem.cout = cout
	err = cn.p.subMap.Add(key, elem)

	if err != nil {
		return nil, errors.New("MQTT5:Connection - Failed to open a channel to key " + key + " - " + err.Error())
	}
	rlog.Debug("[MQTT5:Connection] Subscribed to ", key)
	return cout, nil
}

func (cn *_MQTT5Connection) Unsubscribe(key string) error {
	rlog.Debug("[MQTT5:Connection] Unubscribing from ", key)
	cn.p.lock.RLock()
	defer cn.p.lock.RUnlock()
	usa, err := cn.p.client.Unsubscribe(shared.Ctx, &paho.Unsubscribe{
		Topics: []string{
			key,
		},
	})
	if err != nil {
		return errors.New("MQTT5:Connection - Failed to unsubscribe from " + key + " - " + err.Error() + " : " + fmt.Sprintf("%x", usa.Reasons[0]) + " - " + usa.Properties.ReasonString)
	} else if usa.Reasons[0] != 0x00 {
		return errors.New("MQTT5:Connection - Failed to unsubscribe from " + key + " : " + fmt.Sprintf("%x", usa.Reasons[0]) + " - " + usa.Properties.ReasonString)
	}

	elem, found := cn.p.subMap.Get(key)
	if !found {
		return errors.New("AMQP:Connection - Subscription with key " + key + " not found")
	}
	close(elem.(_MQTT5MapElem).cin)
	cn.p.subMap.Delete(key)
	rlog.Debug("[MQTT5:Connection] Unubscribed from ", key)
	return nil
}

func (cn *_MQTT5Connection) match(topic []string, key []string) bool {
	for idx, val := range topic {
		if val != "#" && val != "+" && val != key[idx] {
			return false
		} else if (val == "+" && len(key) == idx+1) || val == "#" {
			return true
		}
	}
	return len(topic) == len(key)
}

func (cn *_MQTT5Connection) convertChannel(in <-chan *paho.Publish, out chan<- shared.Delivery, key string) {
	keyMap := strings.Split(key, "/")
loop:
	for {
		select {
		case del, ok := <-in:
			if !ok {
				rlog.Debug("[MQTT5:Connection] convertChannel Input channel closed, exiting")
				break loop
			}
			rlog.Debug("[MQTT5:Connection] convertChannel new delivery from ", del.Topic)
			if cn.match(keyMap, strings.Split(del.Topic, "/")) {
				rlog.Debug("[MQTT5:Connection] convertChannel match ", key, " -- ", del.Topic, ", converting delivery")
				out <- MQTT5Delivery{del}
			}
		case <-shared.Ctx.Done():
			rlog.Debug("[MQTT5:Connection] convertChannel Received Context Done, exiting")
			break loop
		}
	}
	close(out)
	shared.Wg.Done()
}

func (cn *_MQTT5Connection) Publish(q string, key string, data []byte) error {
	var brokerCfg *config.BrokerConfig = &config.Static.Broker
	cn.p.lock.RLock()
	defer cn.p.lock.RUnlock()
	_, err := cn.p.client.Publish(shared.Ctx, &paho.Publish{
		Topic:   key,
		QoS:     byte(brokerCfg.MQTT_QoS),
		Retain:  false,
		Payload: data,
	})
	if err != nil {
		return errors.New("MQTT5:Connection - Failed to publish message to key " + key + " - " + err.Error())
	}

	rlog.Debug("[MQTT5:Connection] sent Message from TxChan to topic ", key)
	return nil
}
