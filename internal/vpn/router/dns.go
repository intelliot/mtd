package router

import (
	"bytes"
	"encoding/binary"
	"net"
	"os"
	"strings"

	"github.com/romana/rlog"
)

// DNSHeader describes the request/response DNS header
type DNSHeader struct {
	TransactionID  uint16
	Flags          uint16
	NumQuestions   uint16
	NumAnswers     uint16
	NumAuthorities uint16
	NumAdditionals uint16
}

// DNSResourceRecord describes individual records in the request and response of the DNS payload body
type DNSResourceRecord struct {
	DomainName         string
	Type               uint16
	Class              uint16
	TimeToLive         uint32
	ResourceDataLength uint16
	ResourceData       []byte
}

// Type and Class values for DNSResourceRecord
const (
	TypeA                  uint16 = 1 // a host address
	ClassINET              uint16 = 1 // the Internet
	FlagResponse           uint16 = 1 << 15
	UDPMaxMessageSizeBytes uint   = 512 // RFC1035
)

type DNSServer struct {
	addr   *net.UDPAddr
	conn   *net.UDPConn
	router *Router
}

func (dns *DNSServer) Start(r *Router) {
	var err error
	if r == nil {
		rlog.Critical("[Local DNS] Failed to start: Invalid Router")
		os.Exit(1)
	}
	dns.router = r
	dns.addr, err = net.ResolveUDPAddr("udp", ":35553")
	if err != nil {
		rlog.Critical("[Local DNS] Error resolving UDP address: ", err)
		os.Exit(1)
	}

	dns.conn, err = net.ListenUDP("udp", dns.addr)
	if err != nil {
		rlog.Critical("[Local DNS] Error listening: ", err)
		os.Exit(1)
	}

	rlog.Info("[Local DNS] Listening at ", dns.addr.String())

	go dns.reader()

}

func (dns *DNSServer) Stop() {
	dns.conn.Close()
}

func (dns *DNSServer) reader() {

	for {
		requestBytes := make([]byte, UDPMaxMessageSizeBytes)

		_, clientAddr, err := dns.conn.ReadFromUDP(requestBytes)

		if err != nil {
			rlog.Error("[Local DNS] Error receiving request: ", err)
		} else {
			rlog.Debug("[Local DNS] Received request from ", clientAddr.String())
			go dns.handleDNSClient(requestBytes, clientAddr) // array is value type (call-by-value), i.e. copied
		}
	}
}

// RFC1035: "Domain names in messages are expressed in terms of a sequence
// of labels. Each label is represented as a one octet length field followed
// by that number of octets.  Since every domain name ends with the null label
// of the root, a domain name is terminated by a length byte of zero."
func (dns *DNSServer) readDomainName(requestBuffer *bytes.Buffer) (string, error) {
	var domainName string

	b, err := requestBuffer.ReadByte()

	for ; b != 0 && err == nil; b, err = requestBuffer.ReadByte() {
		labelLength := int(b)
		labelBytes := requestBuffer.Next(labelLength)
		labelName := string(labelBytes)

		if len(domainName) == 0 {
			domainName = labelName
		} else {
			domainName += "." + labelName
		}
	}

	return domainName, err
}

// RFC1035: "Domain names in messages are expressed in terms of a sequence
// of labels. Each label is represented as a one octet length field followed
// by that number of octets.  Since every domain name ends with the null label
// of the root, a domain name is terminated by a length byte of zero."
func (dns *DNSServer) writeDomainName(responseBuffer *bytes.Buffer, domainName string) error {
	labels := strings.Split(domainName, ".")

	for _, label := range labels {
		labelLength := len(label)
		labelBytes := []byte(label)

		responseBuffer.WriteByte(byte(labelLength))
		responseBuffer.Write(labelBytes)
	}

	err := responseBuffer.WriteByte(byte(0))

	return err
}

func (dns *DNSServer) dbLookup(queryResourceRecord DNSResourceRecord) ([]DNSResourceRecord, []DNSResourceRecord, []DNSResourceRecord) {
	var answerResourceRecords = make([]DNSResourceRecord, 0)
	var authorityResourceRecords = make([]DNSResourceRecord, 0)
	var additionalResourceRecords = make([]DNSResourceRecord, 0)

	if queryResourceRecord.Type != TypeA || queryResourceRecord.Class != ClassINET {
		return answerResourceRecords, authorityResourceRecords, additionalResourceRecords
	}

	rt, found := dns.router.GetRouteByName(queryResourceRecord.DomainName)

	if found {
		record := DNSResourceRecord{
			DomainName:         rt.NodeName,
			Type:               TypeA,
			Class:              ClassINET,
			TimeToLive:         31337,
			ResourceData:       net.ParseIP(rt.LocalIP)[12:16], // ipv4 address
			ResourceDataLength: 4,
		}
		answerResourceRecords = append(answerResourceRecords, record)
		rlog.Debug("[Local DNS] ", queryResourceRecord.DomainName, " resolved to ", rt.LocalIP, " ", record)
	}

	return answerResourceRecords, authorityResourceRecords, additionalResourceRecords
}

func (dns *DNSServer) handleDNSClient(requestBytes []byte, clientAddr *net.UDPAddr) {
	var requestBuffer = bytes.NewBuffer(requestBytes)
	var queryHeader DNSHeader
	var queryResourceRecords []DNSResourceRecord

	err := binary.Read(requestBuffer, binary.BigEndian, &queryHeader) // network byte order is big endian
	if err != nil {
		rlog.Error("[Local DNS] Error decoding header: ", err)
	}

	queryResourceRecords = make([]DNSResourceRecord, queryHeader.NumQuestions)

	for idx := range queryResourceRecords {
		queryResourceRecords[idx].DomainName, err = dns.readDomainName(requestBuffer)
		if err != nil {
			rlog.Error("[Local DNS] Error decoding header: ", err)
		}

		queryResourceRecords[idx].Type = binary.BigEndian.Uint16(requestBuffer.Next(2))
		queryResourceRecords[idx].Class = binary.BigEndian.Uint16(requestBuffer.Next(2))
	}

	/**
	 * lookup values
	 */
	var answerResourceRecords = make([]DNSResourceRecord, 0)
	var authorityResourceRecords = make([]DNSResourceRecord, 0)
	var additionalResourceRecords = make([]DNSResourceRecord, 0)

	for _, queryResourceRecord := range queryResourceRecords {
		newAnswerRR, newAuthorityRR, newAdditionalRR := dns.dbLookup(queryResourceRecord)

		answerResourceRecords = append(answerResourceRecords, newAnswerRR...) // three dots cause the two lists to be concatenated
		authorityResourceRecords = append(authorityResourceRecords, newAuthorityRR...)
		additionalResourceRecords = append(additionalResourceRecords, newAdditionalRR...)
	}

	/**
	 * write response
	 */
	var responseBuffer = new(bytes.Buffer)

	responseHeader := DNSHeader{
		TransactionID:  queryHeader.TransactionID,
		Flags:          FlagResponse,
		NumQuestions:   queryHeader.NumQuestions,
		NumAnswers:     uint16(len(answerResourceRecords)),
		NumAuthorities: uint16(len(authorityResourceRecords)),
		NumAdditionals: uint16(len(additionalResourceRecords)),
	}

	err = binary.Write(responseBuffer, binary.BigEndian, &responseHeader)

	if err != nil {
		rlog.Error("[Local DNS] Error writing to buffer: ", err)
	}

	for _, queryResourceRecord := range queryResourceRecords {
		err = dns.writeDomainName(responseBuffer, queryResourceRecord.DomainName)

		if err != nil {
			rlog.Error("[Local DNS] Error writing to buffer: ", err)
		}

		binary.Write(responseBuffer, binary.BigEndian, queryResourceRecord.Type)
		binary.Write(responseBuffer, binary.BigEndian, queryResourceRecord.Class)
	}

	for _, answerResourceRecord := range answerResourceRecords {
		err = dns.writeDomainName(responseBuffer, answerResourceRecord.DomainName)

		if err != nil {
			rlog.Error("[Local DNS] Error writing to buffer: ", err)
		}

		binary.Write(responseBuffer, binary.BigEndian, answerResourceRecord.Type)
		binary.Write(responseBuffer, binary.BigEndian, answerResourceRecord.Class)
		binary.Write(responseBuffer, binary.BigEndian, answerResourceRecord.TimeToLive)
		binary.Write(responseBuffer, binary.BigEndian, answerResourceRecord.ResourceDataLength)
		binary.Write(responseBuffer, binary.BigEndian, answerResourceRecord.ResourceData)
	}

	for _, authorityResourceRecord := range authorityResourceRecords {
		err = dns.writeDomainName(responseBuffer, authorityResourceRecord.DomainName)

		if err != nil {
			rlog.Error("[Local DNS] Error writing to buffer: ", err)
		}

		binary.Write(responseBuffer, binary.BigEndian, authorityResourceRecord.Type)
		binary.Write(responseBuffer, binary.BigEndian, authorityResourceRecord.Class)
		binary.Write(responseBuffer, binary.BigEndian, authorityResourceRecord.TimeToLive)
		binary.Write(responseBuffer, binary.BigEndian, authorityResourceRecord.ResourceDataLength)
		binary.Write(responseBuffer, binary.BigEndian, authorityResourceRecord.ResourceData)
	}

	for _, additionalResourceRecord := range additionalResourceRecords {
		err = dns.writeDomainName(responseBuffer, additionalResourceRecord.DomainName)

		if err != nil {
			rlog.Error("[Local DNS] Error writing to buffer: ", err)
		}

		binary.Write(responseBuffer, binary.BigEndian, additionalResourceRecord.Type)
		binary.Write(responseBuffer, binary.BigEndian, additionalResourceRecord.Class)
		binary.Write(responseBuffer, binary.BigEndian, additionalResourceRecord.TimeToLive)
		binary.Write(responseBuffer, binary.BigEndian, additionalResourceRecord.ResourceDataLength)
		binary.Write(responseBuffer, binary.BigEndian, additionalResourceRecord.ResourceData)
	}

	rlog.Debug("[Local DNS] sending: ", responseBuffer.String())
	dns.conn.WriteToUDP(responseBuffer.Bytes(), clientAddr)
}
