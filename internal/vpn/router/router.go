package router

import (
	"net"
	"os"
	"sync"

	"github.com/romana/rlog"
)

type Route struct {
	NodeName   string
	LocalIP    string
	ExternalIP string
}

type Router struct {
	sync.RWMutex
	NodeName string
	ExtIP    string
	LocIP    string

	routesLocal    map[string]Route
	routesExternal map[string]Route
	routesByName   map[string]Route
}

func (r *Router) Start() {
	r.routesLocal = make(map[string]Route)
	r.routesExternal = make(map[string]Route)
	r.routesByName = make(map[string]Route)
}

func (r *Router) GetRouteFromLocalIP(l net.IP) (net.IP, bool) {
	r.RLock()
	defer r.RUnlock()

	if r.LocIP == l.String() {
		// if we receive our own local IP as dst, we return it as is and bypass encrypt-decrypt
		return net.ParseIP(r.LocIP), true
	} else {
		rt, found := r.routesLocal[l.String()]
		return net.ParseIP(rt.ExternalIP), found
	}
}

func (r *Router) GetRouteFromExternalIP(ext net.IP) (net.IP, bool) {
	r.RLock()
	defer r.RUnlock()

	if r.ExtIP == ext.String() {
		// if we receive our own local IP as dst, we return it as is and bypass encrypt-decrypt
		return net.ParseIP(r.ExtIP), true
	} else {
		rt, found := r.routesExternal[ext.String()]
		return net.ParseIP(rt.LocalIP), found
	}
}

func (r *Router) GetRouteByName(n string) (Route, bool) {
	r.RLock()
	defer r.RUnlock()

	rt, found := r.routesByName[n]
	return rt, found
}

func (r *Router) GetAllLocalRoutes() chan net.IP {
	out := make(chan net.IP)
	go func(rc chan<- net.IP) {
		r.RLock()
		defer r.RUnlock()
		for _, rt := range r.routesLocal {
			rc <- net.ParseIP(rt.ExternalIP)
		}
		close(rc)
	}(out)
	return out
}

func (r *Router) Update(newExtIP string, newCIDR string, RoutesToAdd []Route, RoutesToRemove []string) {
	r.Lock()
	defer r.Unlock()
	ip, _, err := net.ParseCIDR(newCIDR)
	if err != nil {
		rlog.Critical("[Router] Error: ", err)
		os.Exit(1)
	}

	r.ExtIP = newExtIP
	r.LocIP = ip.String()

	//first remove invalid routes
	r.removeRoutes(RoutesToRemove)
	//then add new routes and update existing routes
	r.addRoutes(RoutesToAdd)
}

func (r *Router) addRoutes(newRoutes []Route) {
	// no need for lock, addRoutes and removeRoutes run inside lock
	// we search routes by NodeName, which we consider unique and unchangeable
	// we assume the server has generated a perfect RT without collisions

	for _, rt := range newRoutes {
		// first we check if the node we are trying to add already exists
		oldRt, foundByN := r.routesByName[rt.NodeName]
		// then we add the new entry or update the existing entry by name
		r.routesByName[rt.NodeName] = rt

		// then we try to cleanup the other two maps
		if foundByN {
			delete(r.routesLocal, oldRt.LocalIP)
			delete(r.routesExternal, oldRt.ExternalIP)
		}
		rlog.Debug("[Router] 1st pass Added route ", rt, " to routesByName")
	}
	// to add the new routes to routesLocal and routesExternal we have to complete the cleanup
	// to avoid deleting newly added routes by accident
	for _, rt := range newRoutes {
		// now that the routesLocal and routesExternal maps are clean from old routes,
		// we add the new routes
		r.routesLocal[rt.LocalIP] = rt
		r.routesExternal[rt.ExternalIP] = rt
		rlog.Debug("[Router] 2st pass Added route ", rt, " to routesLocal and routesExternal")
	}
}

func (r *Router) removeRoutes(rmRoutes []string) {
	// no need for lock, addRoutes and removeRoutes run inside lock
	// iterate over the array and try to delete all key-value pairs
	// we search routes by NodeName, which we consider unique and unchangeable
	for _, oldNodeName := range rmRoutes {
		oldRt, found := r.routesByName[oldNodeName]
		if found {
			// delete the old key-value pair from the old maps
			delete(r.routesLocal, oldRt.LocalIP)
			delete(r.routesExternal, oldRt.ExternalIP)
			delete(r.routesByName, oldRt.NodeName)
			rlog.Debug("[Router]  Removed route ", oldRt)
		}
	}
}
