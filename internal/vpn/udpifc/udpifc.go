package udpifc

import (
	"errors"
	"fmt"
	"io"
	"mtd/internal/vpn/shared"
	"net"
	"sync"

	"github.com/romana/rlog"
)

type safeBool struct {
	lock   sync.RWMutex
	enable bool
}

func (sb *safeBool) set(val bool) {
	sb.lock.Lock()
	defer sb.lock.Unlock()
	sb.enable = val
}

func (sb *safeBool) check() bool {
	sb.lock.RLock()
	defer sb.lock.RUnlock()
	return sb.enable
}

type udpConn struct {
	Port       int
	Protoc     string
	conn       *net.UDPConn
	Tx         chan shared.IPPacketWithAddress
	Rx         chan shared.IPPacket
	stopWriter chan struct{}
	//reader stops by closing conn
}

func NewUDPConn() (*udpConn, error) {
	newConn := new(udpConn)
	newConn.Tx = make(chan shared.IPPacketWithAddress)
	newConn.Rx = make(chan shared.IPPacket)
	newConn.stopWriter = make(chan struct{})
	newConn.conn = nil

	return newConn, nil
}

func (udp *udpConn) Start(Port int, Protoc string) error {
	if !udp.IsStopped() {
		udp.Stop()
	}

	udp.Port = Port
	udp.Protoc = Protoc
	rlog.Tracef(1, "[UDPConn] Starting UDP connection: %d, %s", udp.Port, udp.Protoc)

	udp.Port = Port
	udp.Protoc = Protoc
	addrUDP, err := net.ResolveUDPAddr(Protoc, fmt.Sprintf(":%v", Port))
	if err != nil {
		return errors.New("unable to resolve UDP address: " + err.Error())
	}
	udp.conn, err = net.ListenUDP(Protoc, addrUDP)
	if err != nil {
		return errors.New("unable to create UDP socket: " + err.Error())
	}
	go udp.Writer()
	go udp.Reader()

	return nil
}

func (udp *udpConn) isEOF(err error) bool {
	if err == nil {
		return false
	} else if err == io.EOF {
		return true
	} else if oerr, ok := err.(*net.OpError); ok {
		/* this hack happens because the error is returned when the
		 * network socket is closing and instead of returning a
		 * io.EOF it returns this error.New(...) struct. */
		if oerr.Err.Error() == "use of closed network connection" {
			return true
		} else if oerr.Err.Error() == "connection reset by peer" {
			return true
		}
	} else if err.Error() == "use of closed network connection" {
		return true
	}
	return false
}

func (udp *udpConn) Writer() {
	for {
		select {
		case <-udp.stopWriter:
			return
		case b := <-udp.Tx:
			addrUDP, err := net.ResolveUDPAddr(udp.Protoc, fmt.Sprintf("%v:%v", b.Addr.String(), udp.Port))
			if err != nil {
				rlog.Error("[UDPconn] writer: Error resolving address: ", err)
			}
			n, err := udp.conn.WriteToUDP(b.IPPacket, addrUDP)

			for n < len(b.IPPacket) || err != nil {
				if err != nil {
					rlog.Error("[UDPconn] Writer error: ", err)
					break
				}
				n, err = udp.conn.WriteToUDP(b.IPPacket[n:], addrUDP)
			}
		}
	}
}

func (udp *udpConn) Reader() {
	b := make([]byte, shared.BUFFERSIZE)
	EOF := false
	for !EOF {
		n, err := udp.conn.Read(b)
		if udp.isEOF(err) {
			EOF = true
		} else if err != nil {
			rlog.Error("[UDPconn] Reader error: ", err)
		}
		if n > 0 {
			out := make([]byte, n)
			copy(out, b[:n])
			udp.Rx <- out
		}
	}
}

func (udp *udpConn) Stop() {
	rlog.Tracef(1, "[UDPConn] Stopping UDP connection: %d, %s", udp.Port, udp.Protoc)
	udp.stopWriter <- struct{}{}
	err := udp.conn.Close() //this sends EOF and stops the reader routine
	if err != nil {
		rlog.Error("[UDPconn] Stop error: ", err)
	}
	udp.conn = nil
}

func (udp *udpConn) IsStopped() bool {
	return udp.conn == nil
}

func (udp *udpConn) String() string {
	status := "Active"
	if udp.IsStopped() {
		status = "Stopped"
	}
	return "Port: " + fmt.Sprint(udp.Port) + ", Protoc: " + udp.Protoc + ", status: " + status
}

type UDP struct {
	In  chan shared.IPPacketWithAddress
	Out chan shared.IPPacket

	curr *udpConn
	prev *udpConn

	lock sync.RWMutex
}

func (ifc *UDP) Configure(Port int, Protoc string) error {
	ifc.lock.Lock()

	rlog.Tracef(1, "[UDP] New UDP connection: %d, %s", Port, Protoc)
	// check curr first
	if ifc.curr.IsStopped() {
		//if curr is stopped, start it
		err := ifc.curr.Start(Port, Protoc)
		if err != nil {
			return errors.New("unable to start UDP connection: " + err.Error())
		}
	} else if ifc.curr.Port != Port || ifc.curr.Protoc != Protoc {
		//if curr is different and active, move it to prev
		//first close prev
		if !ifc.prev.IsStopped() {
			ifc.prev.Stop()
		}
		//then keep curr values and update curr to new
		prevPort := ifc.curr.Port
		prevProtoc := ifc.curr.Protoc
		err := ifc.curr.Start(Port, Protoc)
		if err != nil {
			return errors.New("unable to create UDP connection: " + err.Error())
		}
		//finally update prev to old curr
		err = ifc.prev.Start(prevPort, prevProtoc)
		if err != nil {
			return errors.New("unable to create UDP connection: " + err.Error())
		}
	}
	ifc.lock.Unlock()
	return nil
}

func (ifc *UDP) Start() {
	ifc.In = make(chan shared.IPPacketWithAddress, 10)
	ifc.Out = make(chan shared.IPPacket, 10)

	var err error
	// first run init
	if ifc.curr == nil {
		ifc.curr, err = NewUDPConn()
		if err != nil {
			rlog.Error("[UDP] unable to create UDP connection: " + err.Error())
		}
	}
	if ifc.prev == nil {
		ifc.prev, err = NewUDPConn()
		if err != nil {
			rlog.Error("[UDP] unable to create UDP connection: " + err.Error())
		}
	}

	go ifc.reader()
	go ifc.writer()
}

func (ifc *UDP) reader() {
	for {
		// even if the connections are inactive, the channels are valid
		select {
		case b := <-ifc.curr.Rx:
			ifc.Out <- b
		case b := <-ifc.prev.Rx:
			ifc.Out <- b
		}
	}
}

func (ifc *UDP) writer() {
	for {
		ifc.curr.Tx <- <-ifc.In
	}
}
