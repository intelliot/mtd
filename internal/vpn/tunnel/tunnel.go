package tunnel

import (
	"encoding/binary"
	"errors"
	"fmt"
	"mtd/internal/vpn/shared"
	"net"
	"sync"
	"time"

	"github.com/romana/rlog"
	"github.com/songgao/water"
	"github.com/vishvananda/netlink"
)

func parseCIDR(CIDR string) (net.IP, *net.IPNet, net.IP, error) {
	lip, lnet, err := net.ParseCIDR(CIDR)
	if err != nil {
		return nil, nil, nil, err
	}
	if lnet.IP.To4() == nil {
		return nil, nil, nil, errors.New("does not support IPv6 addresses for now")
	}
	bcastip := make(net.IP, len(lnet.IP.To4()))
	binary.BigEndian.PutUint32(bcastip, binary.BigEndian.Uint32(lnet.IP.To4())|^binary.BigEndian.Uint32(net.IP(lnet.Mask).To4()))
	return lip, lnet, bcastip, nil
}

type tunState struct {
	sync.RWMutex
	CIDR    string
	ip      net.IP
	net     *net.IPNet
	bcastip net.IP
	iflink  *netlink.Link
}

func (cfg *tunState) copy() *tunState {
	cfg.RLock()
	defer cfg.RUnlock()
	return &tunState{
		CIDR:    cfg.CIDR,
		ip:      cfg.ip,
		net:     cfg.net,
		bcastip: cfg.bcastip,
		iflink:  cfg.iflink,
	}
}

func (cfg *tunState) getLinkIndex() int {
	cfg.RLock()
	defer cfg.RUnlock()
	return (*cfg.iflink).Attrs().Index
}

func (cfg *tunState) setNet(newNet *net.IPNet) {
	cfg.Lock()
	defer cfg.Unlock()
	cfg.net = newNet
}

func (cfg *tunState) hasNet() bool {
	cfg.RLock()
	defer cfg.RUnlock()
	return cfg.net != nil
}

func (cfg *tunState) netString() string {
	cfg.RLock()
	defer cfg.RUnlock()
	return cfg.net.String()
}

func (cfg *tunState) hasIP() bool {
	cfg.RLock()
	defer cfg.RUnlock()
	return !(cfg.ip == nil || cfg.ip.IsUnspecified())
}

func (cfg *tunState) resetIP() {
	cfg.Lock()
	defer cfg.Unlock()
	cfg.ip = nil
}

func (cfg *tunState) resetBcast() {
	cfg.Lock()
	defer cfg.Unlock()
	cfg.bcastip = nil
}

func (cfg *tunState) resetCIDR() {
	cfg.Lock()
	defer cfg.Unlock()
	cfg.CIDR = ""
}

func (cfg *tunState) IsBcast(ip net.IP) bool {
	cfg.RLock()
	defer cfg.RUnlock()
	return cfg.bcastip.Equal(ip)
}

func (cfg *tunState) IsIp(ip net.IP) bool {
	cfg.RLock()
	defer cfg.RUnlock()
	return cfg.ip.Equal(ip)
}

func (cfg *tunState) String() string {
	cfg.RLock()
	defer cfg.RUnlock()
	return cfg.CIDR
}

func (cfg *tunState) getPrevRoute() (string, *netlink.Route) {
	cfg.RLock()
	defer cfg.RUnlock()
	route := &netlink.Route{
		Dst:       cfg.net,
		LinkIndex: (*cfg.iflink).Attrs().Index,
	}
	return cfg.net.String(), route
}

func (cfg *tunState) addNewAddr(addr *netlink.Addr) error {
	cfg.RLock()
	defer cfg.RUnlock()
	err := netlink.AddrAdd(*cfg.iflink, addr)
	if err != nil {
		return fmt.Errorf("error adding new address: %v", err)
	}
	return nil
}

func (cfg *tunState) remPrevAddr(addr *netlink.Addr) error {
	cfg.RLock()
	defer cfg.RUnlock()
	//delete prev addr
	err := netlink.AddrDel(*cfg.iflink, addr)
	if err != nil {
		return fmt.Errorf("error deleting prev address: %v", err)
	}
	return nil
}

func (cfg *tunState) addNewCIDR(cidr string) error {
	cfg.Lock()
	defer cfg.Unlock()
	cfg.CIDR = cidr
	ip, net, bcastip, err := parseCIDR(cidr)
	if err != nil {
		return err
	}
	cfg.ip = ip
	cfg.net = net
	cfg.bcastip = bcastip
	return nil
}

func (cfg *tunState) addNet(newNet *net.IPNet, addRoute bool) error {
	if newNet != nil {
		if addRoute {
			//create new route
			rlog.Debug("[Tunnel] Adding net route ", newNet.String())
			route := &netlink.Route{
				Dst:       newNet,
				LinkIndex: cfg.getLinkIndex(),
			}
			//add new route
			err := netlink.RouteAdd(route)
			if err != nil {
				return fmt.Errorf("error adding new route %v: %v", route, err)
			}
		}
		cfg.setNet(newNet)
	}
	return nil
}

func (cfg *tunState) removeNet(removeRoute bool) error {
	if cfg.hasNet() {
		if removeRoute {
			name, route := cfg.getPrevRoute()
			rlog.Debug("[Tunnel] Deleting net route ", name)
			//delete prev route
			err := netlink.RouteDel(route)
			if err != nil {
				return fmt.Errorf("error deleting prev route: %v", err)
			}
		}
		cfg.setNet(nil)
	}
	return nil
}

func (cfg *tunState) addIP(newCIDR string, addToIfc bool) error {
	if newCIDR != "" {
		if addToIfc {
			//parse new addr
			rlog.Debug("[Tunnel] Adding ip ", newCIDR)
			newAddr, err := netlink.ParseAddr(newCIDR)
			if err != nil {
				return fmt.Errorf("error parsing new address: %v", err)
			}

			err = cfg.addNewAddr(newAddr)
			if err != nil {
				return err
			}
		}
	}
	return cfg.addNewCIDR(newCIDR)
}

func (cfg *tunState) removeIP(removeFromIfc bool) error {
	if cfg.hasIP() {
		if removeFromIfc {
			//parse prev addr
			rlog.Debug("[Tunnel] Deleting ip ", cfg.String())
			prevAddr, err := netlink.ParseAddr(cfg.String())
			if err != nil {
				return fmt.Errorf("error parsing prev address: %v", err)
			}

			cfg.remPrevAddr(prevAddr)
		}

		cfg.resetIP()
		cfg.resetBcast()
		cfg.resetCIDR()
	}
	return nil
}

type TUN struct {
	sync.RWMutex
	In  chan shared.IPPacket
	Out chan shared.IPPacket

	curr *tunState
	prev *tunState

	ifptr *water.Interface
}

func (t *TUN) String() string {
	return "Interface " + t.ifptr.Name() + ", curr cfg:" + t.curr.String() + ", prev cfg:" + t.prev.String()
}

func (t *TUN) Start(ifptr *water.Interface, iflink *netlink.Link) {
	t.In = make(chan shared.IPPacket, 10)
	t.Out = make(chan shared.IPPacket, 10)
	t.curr = new(tunState)
	t.prev = new(tunState)

	t.ifptr = ifptr
	t.curr.iflink = iflink
	t.prev.iflink = iflink

	// for i := 1; i <= 10; i++ {
	go t.reader()
	go t.writer()
	// }
}

func (t *TUN) Configure(newCIDR string) error {
	t.Lock()
	defer t.Unlock()
	clearprevNet := false
	clearprevIP := false
	addNewNet := false
	addNewIP := false

	lip, lnet, _, err := parseCIDR(newCIDR)
	if err != nil {
		return err
	}
	//clear prev state, should already be clean, check just in case

	//check if the prev net is valid and different from both the current and the new
	if t.prev.hasNet() &&
		t.prev.netString() != t.curr.netString() &&
		t.prev.netString() != lnet.String() {
		clearprevNet = true
	}

	//check if the prev ip is valid and different from both the current and the new
	if t.prev.hasIP() &&
		!t.prev.IsIp(t.curr.ip) &&
		!t.prev.IsIp(lip) {
		clearprevIP = true
	}

	//init new state
	//check if the new net is different from the prev and curr nets
	if lnet != nil &&
		(t.curr.hasNet() || lnet.String() != t.curr.netString()) &&
		(t.prev.hasNet() || lnet.String() != t.prev.netString()) {
		addNewNet = true
	}

	//check if the new ip is different from the prev and curr ips
	if !(lip == nil || lip.IsUnspecified()) &&
		!t.curr.IsIp(lip) &&
		!t.prev.IsIp(lip) {
		addNewIP = true
	}

	//update prev state
	err = t.prev.removeNet(clearprevNet)
	if err != nil {
		return err
	}

	err = t.prev.removeIP(clearprevIP)
	if err != nil {
		return err
	}

	t.prev = t.curr.copy()

	//update curr state
	err = t.curr.addNet(lnet, addNewNet)
	if err != nil {
		return err
	}

	err = t.curr.addIP(newCIDR, addNewIP)
	if err != nil {
		return err
	}

	//start timers to close prev connections
	//if prev is different from current, remove it from TUN as well
	errChan := make(chan error)
	time.AfterFunc(5*time.Second, func() {
		rlog.Debug("[Tunnel] trying to close prev conn")
		err := t.prev.removeNet(t.prev.net != nil && t.prev.net.String() != t.curr.net.String())
		if err != nil {
			rlog.Error("[Tunnel] Failed to close prev conn: ", err)
			errChan <- err
		}
		err = t.prev.removeIP(t.prev.ip != nil && !t.prev.ip.Equal(t.curr.ip))
		if err != nil {
			rlog.Error("[Tunnel] Failed to close prev conn: ", err)
			errChan <- err
		}
		errChan <- nil
	})

	return <-errChan
}

func (t *TUN) IsBcast(ip net.IP) bool {
	t.RLock()
	defer t.RUnlock()
	return t.curr.IsBcast(ip) || t.prev.IsBcast(ip)
}

func (t *TUN) reader() {
	b := make([]byte, shared.BUFFERSIZE)
	for {
		n, err := t.ifptr.Read(b[:shared.MTU])
		if err != nil {
			rlog.Error("[TUN reader] Error reading packet: ", err)
		} else if n > 0 {
			out := make([]byte, n)
			copy(out, b[:n])
			t.Out <- out
		}
	}
}

func (t *TUN) writer() {
	for {
		b := <-t.In
		n, err := t.ifptr.Write([]byte(b))
		if err != nil {
			rlog.Error("[TUN writer] Error writing packet: ", err)
		}
		for n != len(b) {
			rlog.Error("[TUN writer] Only ", n, " bytes of ", len(b), " written")
			n2, err := t.ifptr.Write([]byte(b)[n:])
			if err != nil {
				rlog.Error("[TUN writer] Error writing packet: ", err)
			}
			n += n2
		}
		rlog.Debug("[TUN writer] wrote packet: ", len(b), "B")
	}
}
