package vpn

import (
	"fmt"
	"mtd/internal/config"
	"mtd/internal/vpn/cipher"
	"mtd/internal/vpn/router"
	"mtd/internal/vpn/shared"
	"mtd/internal/vpn/tunnel"
	"mtd/internal/vpn/udpifc"
	"mtd/internal/vpn/workers"
	"net"
	"os"
	"sync"

	"github.com/romana/rlog"
	"github.com/songgao/water"
	"github.com/vishvananda/netlink"
)

var underline = false

type TUNcfg struct {
	CIDR  string
	IsSet bool
}

type UDPcfg struct {
	Port   int
	Protoc string
	IsSet  bool
}

type CipherCfg struct {
	CipherKey  string
	CipherType cipher.CipherType
	IsSet      bool
}

type RouterCfg struct {
	ExtIP          string
	RoutesToAdd    []router.Route
	RoutesToRemove []string
	IsSet          bool
}

type VPNConfig struct {
	TUN    TUNcfg
	UDP    UDPcfg
	Cipher CipherCfg
	Router RouterCfg
}

type vpnState struct {
	lock    sync.RWMutex
	ifName  string
	ifIP    net.IP
	tunName string
	mac     string
	tunPtr  *water.Interface
	tunLink *netlink.Link

	rtr  *router.Router
	dns  *router.DNSServer
	tun  *tunnel.TUN
	udp  *udpifc.UDP
	cphr *cipher.Cipher

	workers    int
	encWorkers []*workers.Encryptor
	decWorkers []*workers.Decryptor
}

var (
	iface vpnState = vpnState{
		tunName: "",
		tunPtr:  nil,
		ifIP:    nil,
	}
)

func Start() error {
	err := iface.Initialize()
	if err != nil {
		return err
	}
	return nil
}

func Update(newCfg *VPNConfig) error {
	return iface.Configure(newCfg)
}

func Stop() error {
	iface.dns.Stop()
	return nil
}

func GetIP() net.IP {
	return iface.ifIP
}

func GetMAC() string {
	mtd0Interface, err := net.InterfaceByName("eth0")
	if err != nil {
		rlog.Critical("[VPN] Cannot open interface: ", err)
		os.Exit(1)
	}
	return mtd0Interface.HardwareAddr.String()
}

func (ifc *vpnState) Initialize() error {
	cfg := &config.Static.VPN

	ifc.lock.Lock()
	defer ifc.lock.Unlock()

	var err error

	if cfg.Interface == "" {
		rlog.Critical("[VPN] Cannot open interface: interface name is empty")
		os.Exit(1)
	}
	ifc.ifName = cfg.Interface
	udpI, err := net.InterfaceByName(ifc.ifName)
	if err != nil {
		rlog.Critical("[VPN] Cannot open interface: ", err)
		os.Exit(1)
	}
	addrs, err := udpI.Addrs()
	if err != nil {
		rlog.Critical("[VPN] Cannot get interface ip: ", err)
		os.Exit(1)
	}
	// handle err
	for _, addr := range addrs {
		var ip net.IP
		switch v := addr.(type) {
		case *net.IPNet:
			ip = v.IP
		case *net.IPAddr:
			ip = v.IP
		}
		if ip.To4() != nil {
			rlog.Debugf("[VPN] Interface %s: ip %s", ifc.ifName, ip)
			iface.ifIP = ip
			break
		}
	}

	if cfg.TunnelName == "" {
		rlog.Warn("[VPN] Tunnel name is empty, using default (mtd0)")
		ifc.tunName = "mtd0"
	} else {
		ifc.tunName = cfg.TunnelName
	}

	//init the actual interface
	//create tun
	ifc.tunPtr, err = water.New(water.Config{
		DeviceType: water.TUN,
		PlatformSpecificParams: water.PlatformSpecificParams{
			Name:       ifc.tunName,
			MultiQueue: true,
		},
	})
	if err != nil {
		return fmt.Errorf("unable to allocate TUN interface (%s): %v", ifc.tunName, err)
	}

	rlog.Infof(config.ClrYellowFg+"[VPN] Interface allocated: %s"+config.ClrReset, ifc.tunPtr.Name())

	//open link to tun ifc
	ifc.tunLink = new(netlink.Link)
	*ifc.tunLink, err = netlink.LinkByName(ifc.tunPtr.Name())
	if err != nil {
		return err
	}

	//setup tun ifc
	netlink.LinkSetMTU(*ifc.tunLink, shared.MTU)
	netlink.LinkSetTxQLen(*ifc.tunLink, 32000)
	netlink.LinkSetUp(*ifc.tunLink)
	//start routines
	//start router
	ifc.rtr = new(router.Router)
	ifc.rtr.Start()

	ifc.dns = new(router.DNSServer)
	ifc.dns.Start(ifc.rtr)

	//start tunnel
	ifc.tun = new(tunnel.TUN)
	ifc.tun.Start(ifc.tunPtr, ifc.tunLink)

	//start udp connection
	ifc.udp = new(udpifc.UDP)
	ifc.udp.Start()

	//start cipher
	ifc.cphr = new(cipher.Cipher)
	ifc.cphr.Start()

	//start workers
	ifc.workers = config.Static.VPN.Workers
	//start decryptors
	ifc.decWorkers = make([]*workers.Decryptor, ifc.workers)

	for w := 0; w < ifc.workers; w++ {
		ifc.decWorkers[w] = new(workers.Decryptor)
		ifc.decWorkers[w].Start(ifc.tun, ifc.udp, ifc.cphr)
	}
	//start encryptors
	ifc.encWorkers = make([]*workers.Encryptor, ifc.workers)
	for w := 0; w < ifc.workers; w++ {
		ifc.encWorkers[w] = new(workers.Encryptor)
		ifc.encWorkers[w].Start(ifc.rtr, ifc.tun, ifc.udp, ifc.cphr, ifc.decWorkers[w].Bypass)
	}

	mtd0Interface, err := net.InterfaceByName("eth0")
	if err != nil {
		rlog.Critical("[VPN] Cannot get MAC address for interface mtd0: ", err)
		os.Exit(1)
	}

	// Print the MAC address of the "mtd0" interface
	rlog.Infof("[VPN] MAC address of mtd0 interface: %s", mtd0Interface.HardwareAddr)
	return nil
}

func (ifc *vpnState) Configure(newCfg *VPNConfig) error {
	ifc.lock.Lock()
	defer ifc.lock.Unlock()

	var err error
	rlog.Debug("[VPN] New Config: TUN ", newCfg.TUN.IsSet, ", UDP ", newCfg.UDP.IsSet, ", Cipher ", newCfg.Cipher.IsSet, ", Router ", newCfg.Router.IsSet)

	if !underline && newCfg.UDP.Port > 30010 {
		rlog.Tracef(1, "\033[4mNew config: %+v"+config.ClrReset, newCfg)
		underline = true
	} else {
		rlog.Tracef(1, "New config: %+v", newCfg)
	}

	if newCfg.TUN.IsSet {
		/*
		 * First Update TUN Interface
		 */
		ifc.tun.Configure(newCfg.TUN.CIDR)
	}

	if newCfg.UDP.IsSet {
		/*
		 * Second Update Udp connection
		 */
		err = ifc.udp.Configure(newCfg.UDP.Port, newCfg.UDP.Protoc)
		if err != nil {
			return err
		}
	}

	if newCfg.Cipher.IsSet {
		/*
		 * Third Update cipher
		 */
		err = ifc.cphr.Configure(newCfg.Cipher.CipherType, newCfg.Cipher.CipherKey)
		if err != nil {
			return err
		}
	}

	if newCfg.Router.IsSet && newCfg.TUN.IsSet {
		/*
		 * Fourth Update router
		 */
		ifc.rtr.Update(newCfg.Router.ExtIP, newCfg.TUN.CIDR, newCfg.Router.RoutesToAdd, newCfg.Router.RoutesToRemove)
	}
	return nil
}
