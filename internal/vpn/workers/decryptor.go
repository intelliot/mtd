package workers

import (
	"mtd/internal/vpn/cipher"
	"mtd/internal/vpn/shared"
	"mtd/internal/vpn/tunnel"
	"mtd/internal/vpn/udpifc"

	"github.com/romana/rlog"
)

type Decryptor struct {
	in     chan shared.IPPacket
	out    chan shared.IPPacket
	Bypass chan shared.IPPacket

	cphr *cipher.Cipher
}

func (dec *Decryptor) Start(tun *tunnel.TUN, udp *udpifc.UDP, cphr *cipher.Cipher) {
	dec.in = udp.Out
	dec.out = tun.In
	dec.Bypass = make(chan shared.IPPacket, 10)

	dec.cphr = cphr

	go dec.decrypt()
}

func (dec *Decryptor) decrypt() {
	var decrypted shared.IPPacket = make([]byte, shared.BUFFERSIZE)
	for {
		select {
		case encrypted := <-dec.in:
			rlog.Debug("[Decryptor] new packet (size ", len(encrypted), "B)")
			if len(encrypted) == 0 {
				continue
			}

			size, err := dec.cphr.Decrypt((*[]byte)(&encrypted), (*[]byte)(&decrypted))
			if err != nil {
				rlog.Error("[Decryptor] Failed to decrypt packet: ", err)
				continue
			}

			rlog.Debug("[Decryptor] Decrypted packet size: ", size, "B")
			dec.out <- decrypted[:size]
			// dec.out <- encrypted
		case encrypted := <-dec.Bypass:
			dec.out <- encrypted
		}
	}
}
