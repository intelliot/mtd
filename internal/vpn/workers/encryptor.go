package workers

import (
	"mtd/internal/vpn/cipher"
	"mtd/internal/vpn/router"
	"mtd/internal/vpn/shared"
	"mtd/internal/vpn/tunnel"
	"mtd/internal/vpn/udpifc"
	"net"

	"github.com/romana/rlog"
	"golang.org/x/net/ipv4"
)

type Encryptor struct {
	in          chan shared.IPPacket
	out         chan shared.IPPacketWithAddress
	bypass      chan shared.IPPacket
	unreachable []net.IP

	rtr  *router.Router
	tun  *tunnel.TUN
	cphr *cipher.Cipher
}

func (enc *Encryptor) Start(rtr *router.Router, tun *tunnel.TUN, udp *udpifc.UDP, cphr *cipher.Cipher, bypass chan shared.IPPacket) {
	enc.in = tun.Out
	enc.out = udp.In
	enc.bypass = bypass
	enc.unreachable = []net.IP{}

	enc.rtr = rtr
	enc.tun = tun
	enc.cphr = cphr

	go enc.encrypt()

}

func (enc *Encryptor) encrypt() {
	encrypted := make([]byte, shared.BUFFERSIZE)
	for {
		packet := <-enc.in
		if packet.IPver() != 4 {
			header, _ := ipv4.ParseHeader(packet)
			rlog.Debug("[Encryptor] Non IPv4 packet ", *header)
			continue
		}
		dst := packet.DstV4()

		addr, ok := enc.rtr.GetRouteFromLocalIP(dst)
		rlog.Debug("[Encryptor] New packet, local dst ", dst, ", external dst ", addr, " (route found: ", ok, ")")

		if ok || enc.tun.IsBcast(dst) || packet.IsMulticast() {

			//the dst is our own ip, forward to ReceiveDecrypt
			if ok && dst.Equal(addr) {
				enc.bypass <- packet
				rlog.Debug("[Encryptor] New packet used bypass")
				continue
			}

			tsize, err := enc.cphr.Encrypt((*[]byte)(&packet), &encrypted)
			if err != nil {
				rlog.Debug("[Encryptor] Failed to encrypt packet: ", err)
			}

			if ok {
				// enc.out <- packetWithAddress{packet, addr}
				enc.out <- shared.IPPacketWithAddress{
					IPPacket: encrypted[:tsize],
					Addr:     addr,
				}
			} else {
				// multicast or broadcast
				for addr := range enc.rtr.GetAllLocalRoutes() {
					// enc.out <- packetWithAddress{packet, addr}
					enc.out <- shared.IPPacketWithAddress{
						IPPacket: encrypted[:tsize],
						Addr:     addr,
					}
				}
			}
		} else {
			if !enc.babis(dst) {
				rlog.Warnf("[Encryptor] Unknown dst: %s", dst)
				enc.unreachable = append(enc.unreachable, dst)
			}
			// TODO: add proxy
		}
	}
}

func (enc *Encryptor) babis(dst net.IP) bool {
	for _, d := range enc.unreachable {
		if d.Equal(dst) {
			return true
		}
	}

	return false
}
