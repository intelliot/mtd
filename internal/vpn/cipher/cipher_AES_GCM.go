package cipher

import (
	"crypto/aes"
	_cipher "crypto/cipher"
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"hash"

	snappy "github.com/golang/snappy"
)

/*
AES CBC cipher
A cipher that encrypts the data with AES-{128, 192, 256} CBC
*/
type AES_GCM_Cipher struct {
	block   _cipher.Block
	key     string
	keySize int
}

func NewAES_GCM(key string) (cipher, error) {

	if key == "" {
		return nil, errors.New("key is empty")
	}

	bkey, err := hex.DecodeString(key)
	if err != nil {
		return nil, errors.New("not valid hex string")
	}

	lbkey := len(bkey)
	if (lbkey != 16) && (lbkey != 24) && (lbkey != 32) {
		return nil, errors.New(`length of key must be 16, 24 or 32 bytes
		(32, 48 or 64 hex symbols)
		to select AES-128, AES-192 or AES-256`)
	}

	cipher := AES_GCM_Cipher{}
	cipher.block, err = aes.NewCipher(bkey)
	if err != nil {
		return nil, err
	}
	cipher.key = key
	cipher.keySize = len(bkey) * 8

	return &cipher, nil
}

func (s *AES_GCM_Cipher) Encrypt(in *[]byte, out *[]byte) (length int, err error) {
	defer func() {
		if erri := recover(); erri != nil {
			err = fmt.Errorf("%v", erri)
		} else {
			err = nil
		}
	}()

	// Never use more than 2^32 random nonces with a given key because of the risk of a repeat.
	nonce := make([]byte, 12)
	// generating random bytes for nonce
	rand.Read(nonce)

	iv := make([]byte, aes.BlockSize)
	//pad the message to be alligned to AES blocksize
	*out = _PKCS5Padding(*in, aes.BlockSize)

	// generating random bytes for IV
	rand.Read(iv)
	// creates encrypter using block and IV
	encrypter := _cipher.NewCBCEncrypter(
		s.block, iv,
	)
	// encrypt the message inplace
	encrypter.CryptBlocks(*out, *out)

	// append the IV and the encrypted message
	*out = append(iv, *out...)

	//explicitly set the return values
	length = len(*out)
	return
}

func (s *AES_GCM_Cipher) Decrypt(in *[]byte, out *[]byte) (length int, err error) {
	defer func() {
		if erri := recover(); erri != nil {
			err = fmt.Errorf("%v", erri)
		} else {
			err = nil
		}
	}()

	iv := (*in)[:aes.BlockSize]
	*out = (*in)[aes.BlockSize:]

	// creates decrypter using block and IV
	decrypter := _cipher.NewCBCDecrypter(s.block, iv)

	// decrypt the message inplace
	decrypter.CryptBlocks(*out, *out)

	//unpad the message
	*out, err = _PKCS5UnPadding(*out)

	length = len(*out)
	return
}

func (s *AES_GCM_Cipher) CheckSize(size int) bool {
	return size > aes.BlockSize && size%aes.BlockSize == 0
}

func (s *AES_GCM_Cipher) GetType() CipherType {
	return AES_GCM
}

func (s *AES_GCM_Cipher) GetKey() string {
	return s.key
}

/*
AES CBC Snappy cipher
A cipher that compresses the data with snappy and encrypts the compressed data with AES-{128, 192, 256} CBC
*/
type AES_GCM_Snappy_Cipher struct {
	block   _cipher.Block
	key     string
	keySize int
}

func NewAES_GCM_Snappy(key string) (cipher, error) {

	if key == "" {
		return nil, errors.New("key is empty")
	}

	bkey, err := hex.DecodeString(key)
	if err != nil {
		return nil, errors.New("not valid hex string")
	}

	lbkey := len(bkey)
	if (lbkey != 16) && (lbkey != 24) && (lbkey != 32) {
		return nil, errors.New(`length of key must be 16, 24 or 32 bytes
		(32, 48 or 64 hex symbols)
		to select AES-128, AES-192 or AES-256`)
	}

	cipher := AES_GCM_Snappy_Cipher{}
	cipher.block, err = aes.NewCipher(bkey)
	if err != nil {
		return nil, err
	}
	cipher.key = key
	cipher.keySize = len(bkey) * 8

	return &cipher, nil
}

func (s *AES_GCM_Snappy_Cipher) Encrypt(in *[]byte, out *[]byte) (length int, err error) {
	defer func() {
		if erri := recover(); erri != nil {
			err = fmt.Errorf("%v", erri)
		}
	}()

	iv := make([]byte, aes.BlockSize)
	// compress the message with snappy
	*out = snappy.Encode(nil, *in)
	//pad the compressed message inplace to be alligned to AES blocksize
	*out = _PKCS5Padding(*out, aes.BlockSize)

	// generating random bytes for IV
	rand.Read(iv)
	// creates encrypter using block and IV
	encrypter := _cipher.NewCBCEncrypter(s.block, iv)
	// encrypt the message inplace
	encrypter.CryptBlocks(*out, *out)

	// append the IV and the encrypted message
	*out = append(iv, *out...)

	length = len(*out)
	return
}

func (s *AES_GCM_Snappy_Cipher) Decrypt(in *[]byte, out *[]byte) (length int, err error) {
	defer func() {
		if erri := recover(); erri != nil {
			err = fmt.Errorf("%v", erri)
		}
	}()

	iv := (*in)[:aes.BlockSize]
	*out = (*in)[aes.BlockSize:]

	// creates decrypter using block and IV
	decrypter := _cipher.NewCBCDecrypter(s.block, iv)

	// decrypt the message inplace
	decrypter.CryptBlocks(*out, *out)

	//unpad the message
	*out, err = _PKCS5UnPadding(*out)

	if err == nil {
		//decompress the message
		*out, err = snappy.Decode(nil, *out)
	}

	length = len(*out)
	return
}

func (s *AES_GCM_Snappy_Cipher) CheckSize(size int) bool {
	return size > aes.BlockSize && size%aes.BlockSize == 0
}

func (s *AES_GCM_Snappy_Cipher) GetType() CipherType {
	return AES_GCM_Snappy
}

func (s *AES_GCM_Snappy_Cipher) GetKey() string {
	return s.key
}

/*
AES CBC + HMAC cipher
A cipher that encrypts the data with AES-{128, 192, 256} CBC and appends the HMAC of the encrypted data
*/
type AES_GCM_HMAC_Cipher struct {
	block    _cipher.Block
	key      string
	keySize  int
	hash     hash.Hash
	hashsize int
}

func NewAES_GCM_HMAC(key string) (cipher, error) {

	if key == "" {
		return nil, errors.New("key is empty")
	}

	bkey, err := hex.DecodeString(key)
	if err != nil {
		return nil, errors.New("not valid hex string")
	}

	lbkey := len(bkey)
	if (lbkey != 48) && (lbkey != 56) && (lbkey != 64) {
		return nil, errors.New(`length of key must be 48, 56 or 64 bytes
		(96, 112 or 128 hex symbols)
		to select AES-128, AES-192 or AES-256 (+ 32 for HMAC sha256 key)`)
	}

	cipher := AES_GCM_HMAC_Cipher{}
	cipher.block, err = aes.NewCipher(bkey[0 : lbkey-32])
	if err != nil {
		return nil, err
	}
	cipher.key = key
	cipher.keySize = (len(bkey) - 32) * 8
	cipher.hash = hmac.New(sha256.New, bkey[lbkey-32:])
	cipher.hashsize = cipher.hash.Size()

	return &cipher, nil
}

func (s *AES_GCM_HMAC_Cipher) Encrypt(in *[]byte, out *[]byte) (length int, err error) {
	defer func() {
		if erri := recover(); erri != nil {
			err = fmt.Errorf("%v", erri)
		}
	}()

	iv := make([]byte, aes.BlockSize)
	// generating random bytes for IV
	rand.Read(iv)

	//pad the message to be alligned to AES blocksize
	*out = _PKCS5Padding(*in, aes.BlockSize)

	if *out != nil {
		// creates encrypter using block and IV
		encrypter := _cipher.NewCBCEncrypter(s.block, iv)
		// encrypt the message inplace
		encrypter.CryptBlocks(*out, *out)

		// append the IV and the encrypted message
		*out = append(iv, *out...)

		// calculate the HMAC of the message
		sum := s.hash.Sum(*out)

		// append the encrypted message and the HMAC
		*out = append(*out, sum[:s.hashsize]...)
	}

	length = len(*out)
	return
}

func (s *AES_GCM_HMAC_Cipher) Decrypt(in *[]byte, out *[]byte) (length int, err error) {
	defer func() {
		if erri := recover(); erri != nil {
			err = errors.New(erri.(string))
		}
	}()

	// remove the appended HMAC from message
	*out = (*in)[:len(*in)-s.hashsize]
	// calculate the new HMAC of the message
	sum_n := s.hash.Sum(*out)
	// compare the old HMAC and the new one
	if !hmac.Equal((*in)[len(*in)-s.hashsize:], sum_n[len(sum_n)-s.hashsize:]) {
		return 0, errors.New("HMAC validation failed")
	}

	iv := (*out)[:aes.BlockSize]
	*out = (*out)[aes.BlockSize:]

	// creates decrypter using block and IV
	decrypter := _cipher.NewCBCDecrypter(s.block, iv)

	// decrypt the message inplace
	decrypter.CryptBlocks(*out, *out)

	//unpad the message
	*out, err = _PKCS5UnPadding(*out)

	length = len(*out)
	return
}

func (s *AES_GCM_HMAC_Cipher) CheckSize(size int) bool {
	return size > (aes.BlockSize+s.hashsize) && size%aes.BlockSize == 0
}

func (s *AES_GCM_HMAC_Cipher) GetType() CipherType {
	return AES_GCM_HMAC
}

func (s *AES_GCM_HMAC_Cipher) GetKey() string {
	return s.key
}

/*
AES CBC + HMAC Snappy cipher
A cipher that compresses the data, encrypts the compressed data with AES-{128, 192, 256} CBC and appends the HMAC of the encrypted data
*/
type AES_GCM_HMAC_Snappy_Cipher struct {
	block    _cipher.Block
	key      string
	keySize  int
	hash     hash.Hash
	hashsize int
}

func NewAES_GCM_HMAC_Snappy(key string) (cipher, error) {

	if key == "" {
		return nil, errors.New("key is empty")
	}

	bkey, err := hex.DecodeString(key)
	if err != nil {
		return nil, errors.New("not valid hex string")
	}

	lbkey := len(bkey)
	if (lbkey != 48) && (lbkey != 56) && (lbkey != 64) {
		return nil, errors.New(`length of key must be 48, 56 or 64 bytes
		(96, 112 or 128 hex symbols)
		to select AES-128, AES-192 or AES-256 (+ 32 for HMAC sha256 key)`)
	}

	cipher := AES_GCM_HMAC_Snappy_Cipher{}
	cipher.block, err = aes.NewCipher(bkey[0 : lbkey-32])
	if err != nil {
		return nil, err
	}
	cipher.key = key
	cipher.keySize = (len(bkey) - 32) * 8
	cipher.hash = hmac.New(sha256.New, bkey[lbkey-32:])
	cipher.hashsize = cipher.hash.Size()

	return &cipher, nil
}

func (s *AES_GCM_HMAC_Snappy_Cipher) Encrypt(in *[]byte, out *[]byte) (length int, err error) {
	defer func() {
		if erri := recover(); erri != nil {
			err = fmt.Errorf("%v", erri)
		}
	}()

	iv := make([]byte, aes.BlockSize)
	// compress the message with snappy
	*out = snappy.Encode(nil, *in)
	//pad the compressed message inplace to be alligned to AES blocksize
	*out = _PKCS5Padding(*out, aes.BlockSize)

	// generating random bytes for IV
	rand.Read(iv)
	// creates encrypter using block and IV
	encrypter := _cipher.NewCBCEncrypter(s.block, iv)
	// encrypt the message inplace
	encrypter.CryptBlocks(*out, *out)

	// append the IV and the encrypted message
	*out = append(iv, *out...)

	// calculate the HMAC of the message
	sum := s.hash.Sum(*out)

	// append the encrypted message and the HMAC
	*out = append(*out, sum[:s.hashsize]...)

	length = len(*out)
	return
}

func (s *AES_GCM_HMAC_Snappy_Cipher) Decrypt(in *[]byte, out *[]byte) (length int, err error) {
	defer func() {
		if erri := recover(); erri != nil {
			err = errors.New(erri.(string))
		}
	}()

	// remove the appended HMAC from message
	*out = (*in)[:len(*in)-s.hashsize]
	// calculate the new HMAC of the message
	sum_n := s.hash.Sum(*out)
	// compare the old HMAC and the new one
	if !hmac.Equal((*in)[len(*in)-s.hashsize:], sum_n[len(sum_n)-s.hashsize:]) {
		return 0, errors.New("HMAC validation failed")
	}

	iv := (*out)[:aes.BlockSize]
	*out = (*out)[aes.BlockSize:]

	// creates decrypter using block and IV
	decrypter := _cipher.NewCBCDecrypter(s.block, iv)

	// decrypt the message inplace
	decrypter.CryptBlocks(*out, *out)

	//unpad the message
	*out, err = _PKCS5UnPadding(*out)
	if err == nil {
		//decompress the message
		*out, err = snappy.Decode(nil, *out)
	}

	length = len(*out)
	return
}

func (s *AES_GCM_HMAC_Snappy_Cipher) CheckSize(size int) bool {
	return size > (aes.BlockSize+s.hashsize) && size%aes.BlockSize == 0
}

func (s *AES_GCM_HMAC_Snappy_Cipher) GetType() CipherType {
	return AES_GCM_HMAC_Snappy
}

func (s *AES_GCM_HMAC_Snappy_Cipher) GetKey() string {
	return s.key
}
