package cipher

import (
	snappy "github.com/golang/snappy"
)

/*
NONE cipher
A cipher that does not modify the data
*/
type NONE_Cipher struct {
	key string
}

func NewNONE(key string) (cipher, error) {
	cipher := NONE_Cipher{}
	cipher.key = key
	return &cipher, nil
}

func (s *NONE_Cipher) Encrypt(in *[]byte, out *[]byte) (int, error) {
	*out = *in
	return len(*out), nil
}

func (s *NONE_Cipher) Decrypt(in *[]byte, out *[]byte) (int, error) {
	*out = *in
	return len(*out), nil
}

func (s *NONE_Cipher) CheckSize(size int) bool {
	return true
}

func (s *NONE_Cipher) GetType() CipherType {
	return NONE
}

func (s *NONE_Cipher) GetKey() string {
	return s.key
}

/*
NONE Snappy cipher
A cipher that compresses the data
*/
type NONE_Snappy_Cipher struct {
	key string
}

func NewNONE_Snappy(key string) (cipher, error) {
	cipher := NONE_Snappy_Cipher{}
	cipher.key = key
	return &cipher, nil
}

func (s *NONE_Snappy_Cipher) Encrypt(in *[]byte, out *[]byte) (int, error) {
	*out = snappy.Encode(nil, *in)
	return len(*out), nil
}

func (s *NONE_Snappy_Cipher) Decrypt(in *[]byte, out *[]byte) (int, error) {
	var err error
	*out, err = snappy.Decode(nil, *in)
	if err != nil {
		return 0, err
	}
	return len(*out), nil
}

func (s *NONE_Snappy_Cipher) CheckSize(size int) bool {
	return true
}

func (s *NONE_Snappy_Cipher) GetType() CipherType {
	return NONE_Snappy
}

func (s *NONE_Snappy_Cipher) GetKey() string {
	return s.key
}
