package cipher

import (
	"bytes"
	"errors"
	"fmt"
	"math/rand"
	"mtd/internal/vpn/shared"

	"github.com/romana/rlog"
)

type CipherType int

type cipher interface {
	Encrypt(in *[]byte, out *[]byte) (int, error)
	Decrypt(in *[]byte, out *[]byte) (int, error)
	CheckSize(size int) bool
	GetType() CipherType
	GetKey() string
}

// TODO: add GCM

const (
	NONE CipherType = iota
	NONE_Snappy
	AES_CBC
	AES_CBC_Snappy
	AES_CBC_HMAC
	AES_CBC_HMAC_Snappy
	AES_GCM
	AES_GCM_Snappy
	AES_GCM_HMAC
	AES_GCM_HMAC_Snappy
	INVALID
)

type newCipherFunc func(string) (cipher, error)

var (
	RegisteredCiphers       = make(map[CipherType]newCipherFunc)
	RegisteredCiphersString = make(map[CipherType]string)

	errPacketWrongPadding = errors.New("packet padding is wrong")
	errPacketSmall        = errors.New("packet too small")
	errPacketNonIPv4      = errors.New("non IPv4 packet")
	errPacketInvalidSize  = errors.New("stored packet size bigger then packet itself")
)

func init() {
	RegisteredCiphers[NONE] = NewNONE
	RegisteredCiphers[NONE_Snappy] = NewNONE_Snappy
	RegisteredCiphers[AES_CBC] = NewAES_CBC
	RegisteredCiphers[AES_CBC_Snappy] = NewAES_CBC_Snappy
	RegisteredCiphers[AES_CBC_HMAC] = NewAES_CBC_HMAC
	RegisteredCiphers[AES_CBC_HMAC_Snappy] = NewAES_CBC_HMAC_Snappy

	RegisteredCiphersString[NONE] = "NONE"
	RegisteredCiphersString[NONE_Snappy] = "NONE with Snappy compression"
	RegisteredCiphersString[AES_CBC] = "AES CBC"
	RegisteredCiphersString[AES_CBC_Snappy] = "AES CBC with Snappy compression"
	RegisteredCiphersString[AES_CBC_HMAC] = "AES CBC with HMAC signature"
	RegisteredCiphersString[AES_CBC_HMAC_Snappy] = "AES CBC with HMAC signature and Snappy compression"
}

var validChars = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@#$-_")

// generates a string of requested size with characters from the validChars array
func RandomString(size int) string {
	byte_array := make([]rune, size)
	for i := range byte_array {
		byte_array[i] = validChars[rand.Intn(len(validChars))]
	}
	return string(byte_array)
}

// PKCS5Padding implements PKCS5 as RFC8018 describes
func _PKCS5Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	// if padding == 0 {
	// 	padding = blockSize
	// }
	return append(ciphertext,
		bytes.Repeat([]byte{uint8(padding)}, padding)...)
}

// PKCS5UnPadding implements PKCS as RFC8018 describes.
func _PKCS5UnPadding(origData []byte) ([]byte, error) {
	length := len(origData)
	unpadding := int(origData[length-1])
	// check if unpadding is inside index range
	if length-unpadding <= 0 {
		return nil, errPacketWrongPadding
	}
	// check if unpadding is the same value for all padded bytes
	for _, up := range origData[(length - unpadding):] {
		if int(up) != unpadding {
			return nil, errPacketWrongPadding
		}
	}
	return origData[:(length - unpadding)], nil
}

type Cipher struct {
	curr cipher
	prev cipher
}

func (c *Cipher) Start() error {
	var err error
	c.curr, err = RegisteredCiphers[NONE]("")
	if err != nil {
		return fmt.Errorf("error creating new cipher: %v", err)
	}

	c.prev, err = RegisteredCiphers[NONE]("")
	if err != nil {
		return fmt.Errorf("error creating new cipher: %v", err)
	}
	return nil
}

func (c *Cipher) Encrypt(in *[]byte, out *[]byte) (int, error) {
	tsize, err := c.curr.Encrypt(in, out)
	if err != nil {
		return 0, err
	}
	return tsize, nil
}

func (c *Cipher) Decrypt(in *[]byte, out *[]byte) (int, error) {

	trycurr := c.curr.CheckSize(len(*in))
	tryprev := c.prev.CheckSize(len(*in))

	if !(trycurr || tryprev) {
		return 0, fmt.Errorf("invalid packet size %vB", len(*in))
	}

	var size int
	var currErr, prevErr error
	if trycurr {
		size, currErr = c.curr.Decrypt(in, out)

		// 2 bytes size + 20 bytes ip header
		if currErr == nil {
			if size < 22 {
				currErr = errPacketSmall
			} else if (*shared.IPPacket)(out).IPver() != 4 {
				currErr = errPacketNonIPv4
			} else {
				psize := (*shared.IPPacket)(out).GetSize()
				if psize > size {
					currErr = errPacketInvalidSize
				}
			}
		}

		if currErr != nil && tryprev {
			size, prevErr = c.prev.Decrypt(in, out)

			// 2 bytes size + 20 bytes ip header
			if prevErr == nil {
				if size < 22 {
					prevErr = errPacketSmall
				} else if (*shared.IPPacket)(out).IPver() != 4 {
					prevErr = errPacketNonIPv4
				} else {
					psize := (*shared.IPPacket)(out).GetSize()
					if psize > size {
						prevErr = errPacketInvalidSize
					}
				}
			}

			if prevErr != nil {
				return 0, fmt.Errorf("failed to decrypt packet: %v - %v", currErr, prevErr)
			}
		} else if currErr != nil {
			return 0, fmt.Errorf("failed to decrypt packet: %v", currErr)
		}
	} else if tryprev {
		size, prevErr = c.prev.Decrypt(in, out)

		// 2 bytes size + 20 bytes ip header
		if prevErr == nil {
			if size < 22 {
				prevErr = errPacketSmall
			} else if (*shared.IPPacket)(out).IPver() != 4 {
				prevErr = errPacketNonIPv4
			} else {
				psize := (*shared.IPPacket)(out).GetSize()
				if psize > size {
					prevErr = errPacketInvalidSize
				}
			}
		}

		if prevErr != nil {
			return 0, fmt.Errorf("failed to decrypt packet: %v", prevErr)
		}
	}

	return size, nil
}

func (c *Cipher) Configure(newType CipherType, newKey string) error {
	rlog.Debug("[Cypher] curr ", c.curr)
	rlog.Debug("[Cypher] prev ", c.prev)
	c.prev = c.curr
	if newType != INVALID &&
		(c.curr.GetKey() != newKey || c.curr.GetType() != newType) {

		var err error
		c.curr, err = RegisteredCiphers[newType](newKey)
		if err != nil {
			// fall back to prev cipher
			c.curr = c.prev
			return fmt.Errorf("error creating new cipher: %v", err)
		}
	}
	rlog.Debug("[Cypher] new curr ", c.curr)
	rlog.Debug("[Cypher] new prev ", c.prev)
	return nil
}
