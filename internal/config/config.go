package config

import (
	"fmt"
	"io/ioutil"
	"net"
	"sync"

	"github.com/romana/rlog"

	"os"

	ctls "crypto/tls"
	"crypto/x509"

	"encoding/json"
)

var (
	ClrReset    = "\033[0m"
	ClrRedFg    = "\033[31m"
	ClrGreenFg  = "\033[32m"
	ClrYellowFg = "\033[33m"
	ClrBlueFg   = "\033[34m"
	ClrPurpleFg = "\033[35m"
	ClrCyanFg   = "\033[36m"
	ClrWhiteFg  = "\033[37m"
	ClrRedBg    = "\033[41m"
	ClrGreenBg  = "\033[42m"
	ClrYellowBg = "\033[43m"
	ClrBlueBg   = "\033[44m"
	ClrPurpleBg = "\033[45m"
	ClrCyanBg   = "\033[46m"
	ClrWhiteBg  = "\033[47m"
)

type LoggerConfig struct {
	Stream string `json:"stream"`
	File   string `json:"file"`
	Level  string `json:"level"`
	Trace  string `json:"trace"`
}

type BrokerConfig struct {
	Protocol string    `json:"protocol"`
	IP       string    `json:"ip"`
	Port     int       `json:"port"`
	User     string    `json:"user"`
	Password string    `json:"password"`
	ClientID string    `json:"clientid"`
	TLS      TLSConfig `json:"tls"`
	MQTT_QoS int       `json:"mqtt_qos"`
	url      string
	lock     sync.RWMutex
}

type TLSConfig struct {
	Enabled         bool   `json:"enabled"`
	Ca_cert_path    string `json:"ca_cert_path"`
	Cert_path       string `json:"cert_path"`
	Key_path        string `json:"key_path"`
	Public_Key_path string `json:"public_key_path"`
	cfg             *ctls.Config
}

type VPNStaticConfig struct {
	//client side
	Interface  string `json:"interface"`
	TunnelName string `json:"tunnel"`
	Workers    int    `json:"workers"`

	//server side
	TunnelCIDR  string `json:"tunnel_cidr"`
	RefreshTime int    `json:"refresh"`
}

type Config struct {
	Logger LoggerConfig    `json:"logger"`
	Broker BrokerConfig    `json:"broker"`
	VPN    VPNStaticConfig `json:"vpn"`
}

func (cfg *BrokerConfig) GetPKey() string {
	key, err := ioutil.ReadFile(cfg.TLS.Public_Key_path)
	if err != nil {
		rlog.Critical("[Config] Failed GetPKEY", err)
	}
	return string(key)
}

func (cfg *BrokerConfig) GetUrl() string {
	cfg.lock.Lock()
	if cfg.url == "" {
		if cfg.Protocol == "mqtt" {
			cfg.url += cfg.IP
			if cfg.Port != 0 {
				cfg.url += ":" + fmt.Sprint(cfg.Port)
			}
		} else if cfg.Protocol == "amqp" {
			if cfg.TLS.Enabled {
				cfg.url = "amqps://"
			} else {
				cfg.url = "amqp://"
			}

			if cfg.User != "" && cfg.Password != "" {
				cfg.url += cfg.User + ":" + cfg.Password + "@"
			}

			cfg.url += cfg.IP
			if cfg.Port != 0 {
				cfg.url += ":" + fmt.Sprint(cfg.Port)
			}
		}
	}
	cfg.lock.Unlock()
	return cfg.url
}

func (cfg *BrokerConfig) GetTls() *ctls.Config {
	cfg.lock.Lock()
	if cfg.TLS.cfg == nil {
		cfg.TLS.cfg = new(ctls.Config)

		if cfg.TLS.Enabled {
			/*
			  we read the CA from the path, alternativelly leave
			  cfg.RootCAs = nil and add the CA via the command line:
			  security add-certificate testca/cacert.pem
			  security add-trusted-cert testca/cacert.pem
			*/

			cfg.TLS.cfg.RootCAs = x509.NewCertPool()
			ca, err := ioutil.ReadFile(cfg.TLS.Ca_cert_path)
			if err != nil {
				rlog.Critical("[Config] Failed to read CA cert: ", err)
				os.Exit(1)
			}
			cfg.TLS.cfg.RootCAs.AppendCertsFromPEM(ca)

			// load client cert and key, needed if rabbitmq config option fail_if_no_peer_cert is true
			if cfg.TLS.Cert_path != "" && cfg.TLS.Key_path != "" {
				cert, err := ctls.LoadX509KeyPair(
					cfg.TLS.Cert_path,
					cfg.TLS.Key_path,
				)
				if err != nil {
					rlog.Critical("[Config] Failed to read cert or key: ", err)
					os.Exit(1)
				}
				cfg.TLS.cfg.Certificates =
					append(cfg.TLS.cfg.Certificates, cert)
			}
		}
	}
	cfg.lock.Unlock()
	return cfg.TLS.cfg
}

var Static Config

func defaultConfig() {
	Static.Logger.File = ""
	Static.Logger.Level = "Info"

	Static.Broker.Protocol = "amqp"
	Static.Broker.ClientID = "ids"
	Static.Broker.IP = "localhost"
	Static.Broker.Port = 5672
	Static.Broker.User = "client"
	Static.Broker.Password = "client"
	Static.Broker.MQTT_QoS = 0

	Static.Broker.TLS.Enabled = false
	Static.Broker.TLS.Ca_cert_path = ""
	Static.Broker.TLS.Cert_path = ""
	Static.Broker.TLS.Key_path = ""
	Static.Broker.TLS.Public_Key_path = ""

	ifaces, err := net.Interfaces()
	if err != nil {
		rlog.Critical("[Config] Error getting network interfaces: ", err)
		os.Exit(1)
	}

	//try to get a valid network interface
	for _, i := range ifaces {
		if i.Flags&net.FlagUp != 0 { // interface has flag UP
			if i.Flags&net.FlagLoopback == 0 { // interface has not flag LOOPBACK
				if a, _ := i.Addrs(); len(a) != 0 { // interface has ip address
					Static.VPN.Interface = i.Name
					break
				}
			}
		}
	}

	Static.VPN.TunnelName = "mtd0"
	Static.VPN.Workers = 1
}

func LoadConfiguration(cfgPath string) {
	var data []byte
	var err error

	if cfgPath != "" {
		data, err = ioutil.ReadFile(cfgPath)
		if err != nil {
			rlog.Critical("[Config] Error reading ", cfgPath, ":", err)
			os.Exit(1)
		}
	}

	defaultConfig()

	if cfgPath != "" {
		err = json.Unmarshal(data, &Static)
		if err != nil {
			rlog.Critical("[Config] Error parsing ", cfgPath, ":", err)
			os.Exit(1)
		}
	}

	if Static.Logger.Stream != "" {
		os.Setenv("RLOG_LOG_STREAM", Static.Logger.Stream)
	}
	if Static.Logger.File != "" {
		os.Setenv("RLOG_LOG_FILE", Static.Logger.File)
	}
	if Static.Logger.Level != "" {
		os.Setenv("RLOG_LOG_LEVEL", Static.Logger.Level)
	}
	if Static.Logger.Trace != "" {
		os.Setenv("RLOG_TRACE_LEVEL", Static.Logger.Trace)
	}

	rlog.UpdateEnv()

	// rlog.Debug("Config: ", Static)

}
