package util

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/romana/rlog"
)

func WaitForInterrupt() {
	c := make(chan os.Signal, 5)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	<-c
	fmt.Println()
	rlog.Info("  --  Ctrl+C pressed in Terminal")
}
