package util

import (
	"errors"
	"mtd/internal/connection/shared"
	"sync"
)

type Map interface {
	Add(key string, elem Elem) error
	Delete(key string) error
	DeleteAll() error
	Has(key string) bool
	Get(key string) (Elem, bool)
	GetAll() (chan Elem, chan bool)
}

type Elem interface{}

type ThreadSafeMap struct {
	cmap  map[string]Elem
	clock sync.RWMutex
}

func NewThreadSafeMap() *ThreadSafeMap {
	newmap := new(ThreadSafeMap)
	newmap.cmap = make(map[string]Elem)
	newmap.clock = sync.RWMutex{}

	return newmap
}

func (c *ThreadSafeMap) Add(key string, elem Elem) error {
	if c.Has(key) {
		return errors.New("AMQP:ThreadSafeMap - key " + key + " already exists, skipping")
	}

	c.clock.Lock()
	c.cmap[key] = elem
	c.clock.Unlock()

	if !c.Has(key) {
		return errors.New("AMQP:ThreadSafeMap - failed to add key " + key)
	}
	return nil
}

func (c *ThreadSafeMap) Delete(key string) error {
	if !c.Has(key) {
		return errors.New("AMQP:ThreadSafeMap - element with key " + key + " not found")
	}

	c.clock.Lock()
	delete(c.cmap, key)
	c.clock.Unlock()

	if c.Has(key) {
		return errors.New("AMQP:ThreadSafeMap - failed to delete key " + key)
	}
	return nil
}

func (c *ThreadSafeMap) DeleteAll() error {
	for key := range c.cmap {
		err := c.Delete(key)
		if err != nil {
			return err
		}
	}
	return nil
}

func (c *ThreadSafeMap) Has(key string) bool {
	c.clock.RLock()
	_, found := c.cmap[key]
	c.clock.RUnlock()
	return found
}

func (c *ThreadSafeMap) Get(key string) (Elem, bool) {
	c.clock.RLock()
	elem, found := c.cmap[key]
	c.clock.RUnlock()
	return elem, found
}

func (c *ThreadSafeMap) GetAll() (chan Elem, chan bool) {
	dataCh := make(chan Elem)
	stopCh := make(chan bool)

	shared.Wg.Add(1)
	go func() {
		c.clock.RLock()
		defer func() {
			c.clock.RUnlock()
			dataCh <- nil
			close(dataCh)
		}()
		for _, elem := range c.cmap {
			select {
			case <-shared.Ctx.Done():
				return
			case <-stopCh:
				return
			case dataCh <- elem:
			}
		}
		shared.Wg.Done()
	}()

	return dataCh, stopCh
}

func (c *ThreadSafeMap) GetLength() uint32 {
	return uint32(len(c.cmap))
}
